close all;
clear all;
clc;

UAV = AerosondeBeard(0, 0);

feedType = {'ABVEULdot', 'NEDdotEULdot'};
for i=1:2
    feed = feedType{i};
        for Noise = 0:1
            for curveType = 1:2
%                 [Result] = TrackOnce(Noise, Noise, curveType, feed, UAV);
                
                path = '/home/leonardo/MATLAB/projects/codigos-dissertacao/Resultados/Aerosonde/';
                if Noise
                    path = strcat(path, 'WithNoise/');
                else
                    path = strcat(path, 'WithoutNoise/');
                end
                
                path = strcat(path, feed(1:3));
                
                if curveType == 1
                    path = strcat(path, '/C1/');
                else
                    path = strcat(path, '/C2/');
                end
                
                cd(path);
                load('Result.mat');
%                 save Result
                
%                 Result = {X; U; E; Re; EKF_OUT, FInfo};
                X = Result{1};
                U = Result{2};
                FInfo = Result{6};
                clc;
                fprintf('%s', path);
                Plots;
                
                clear Result X U FInfo
                close all;
            end
        end
end