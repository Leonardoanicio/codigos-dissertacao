classdef DeHavilland
    properties
        Coefficients
        States
        Controls
        UpperBounds
        LowerBounds
    end
    
    methods
        function obj = DeHavilland()
            load('aeroCoeff');
            load('vehicle');
            obj.Coefficients = aeroCoeff;
            fname = fieldnames(vehicle);
            for i=1:length(fname)
                obj.Coefficients.(fname{i}) = vehicle.(fname{i});
            end
            [Controls, States] = DeHavillandLoad();
            obj.States = num2cell(States);
            obj.Controls = Controls;
            obj.UpperBounds = [vehicle.aileron_max; vehicle.elevator_max; vehicle.rudder_max; vehicle.throttle_max];
            obj.LowerBounds = [vehicle.aileron_min; vehicle.elevator_min; vehicle.rudder_min; vehicle.throttle_min];
        end
        
        function C_6x1 = coeffs_C(obj, qbar, alpha, beta, vt, p, q, r, ThrottleForce)
            %Datum
            datumX =obj.Coefficients.Cx_alpha*alpha +obj.Coefficients.Cx_alpha2*alpha^2 +obj.Coefficients.Cx_alpha3*alpha^3 +obj.Coefficients.Cx0;
            datumY =obj.Coefficients.Cy_beta*beta +obj.Coefficients.Cy0;
            datumZ =obj.Coefficients.Cz_alpha*alpha +obj.Coefficients.Cz_alpha3*alpha^3 +obj.Coefficients.Cz0;
            datuml =obj.Coefficients.Cl_beta*beta +obj.Coefficients.Cl0;
            datumm =obj.Coefficients.Cm_alpha*alpha +obj.Coefficients.Cm_alpha2*alpha^2 +obj.Coefficients.Cm_beta2*beta^2 +obj.Coefficients.Cm0;
            datumn =obj.Coefficients.Cn_beta*beta +obj.Coefficients.Cn_beta3*beta^3 +obj.Coefficients.Cn0;
            datum = [datumX; datumY; datumZ; datuml; datumm; datumn];
            
            %Body damping
            pCoeff = [0; obj.Coefficients.Cy_p; 0; obj.Coefficients.Cl_p; 0; obj.Coefficients.Cn_p]*p*obj.Coefficients.bref/2;
            qCoeff = [obj.Coefficients.Cx_q; 0; obj.Coefficients.Cz_q; 0; obj.Coefficients.Cm_q; obj.Coefficients.Cn_q]*q*obj.Coefficients.cbar;
            rCoeff = [0; obj.Coefficients.Cy_r; 0; obj.Coefficients.Cl_r; obj.Coefficients.Cm_r; obj.Coefficients.Cn_r]*r*obj.Coefficients.bref/2;
            bodyCoeff = pCoeff + qCoeff + rCoeff;
            bodyCoeff = bodyCoeff/vt;
            
            delCoeff = datum + bodyCoeff;
            Forces = qbar*obj.Coefficients.Sref*delCoeff(1:3);
            Moments = qbar*obj.Coefficients.Sref*delCoeff(4:6).*[obj.Coefficients.bref;obj.Coefficients.cbar;obj.Coefficients.bref] + ...
                        cross(Forces, obj.Coefficients.CG - obj.Coefficients.CP)';
            C_6x1 = [Forces; Moments] + [ThrottleForce; zeros(5, 1)];
        end
        
        function M_6x4 = coeffs_M(obj, qbar, alpha, beta, ThrottleCoeff)
            %Aileron
            delAil = [0;obj.Coefficients.Cy_da; 0; alpha*obj.Coefficients.Cl_da_alpha + obj.Coefficients.Cl_da; 0;obj.Coefficients.Cn_da];
            %Elevator
            delElev = [0; 0; beta^2*obj.Coefficients.Cz_de_beta + obj.Coefficients.Cz_de; 0; obj.Coefficients.Cm_de; 0];
            %Rudder
            delRud = [obj.Coefficients.Cx_dr; alpha*obj.Coefficients.Cy_dr_alpha + obj.Coefficients.Cy_dr; 0; obj.Coefficients.Cl_dr; 0; obj.Coefficients.Cn_dr];
            
            delSurf = [delAil, delElev, delRud];
            
            Forces = qbar*obj.Coefficients.Sref*delSurf(1:3, :);
            Moments = qbar*obj.Coefficients.Sref*delSurf(4:6, :).*[obj.Coefficients.bref;obj.Coefficients.cbar;obj.Coefficients.bref];
            
            M_6x4 = [[Forces; Moments], [ThrottleCoeff; zeros(5, 1)]];
        end
        
        function [Coeffs_M, Coeffs_C, Coeffs_P, alpha] = getMatrices(obj)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);

            %Calculo da densidade do ar
            temperatura = 288.15 - 0.0065*(-pd);
            rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);
            qbar = (1/2)*rho*vt^2;
            
            %Interpolação do throttle
            throttle = obj.Controls(4);
            Throttle_y =obj.Coefficients.Thrust*(1+tanh([-3:3]));
            Throttle_x = (1/6)*(3+([-3:3]));
            Throttle_a = interp1(Throttle_x, Throttle_y, throttle + 1e-3, 'linear', 'extrap');
            Throttle_b = interp1(Throttle_x, Throttle_y, throttle - 1e-3, 'linear', 'extrap');
            Throttle = interp1(Throttle_x, Throttle_y, throttle, 'linear', 'extrap');
            
            poly = polyfit([throttle - 1e-3; throttle; throttle + 1e-3], [Throttle_b; Throttle; Throttle_a], 1);
            Throttle_C = poly(2);
            Throttle_M = poly(1);
            
            %Matrizes de forças
            Coeffs_C = obj.coeffs_C(qbar, alpha, beta, vt, p, q, r, Throttle_C);
            Coeffs_M = obj.coeffs_M(qbar, alpha, beta, Throttle_M);
            Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
        end
        
        function [obj, xdot] = EOM(obj, dt, Wind, Winddot)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});
            controls = obj.Controls;
            Aero = obj.Coefficients;
            
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);
            
            %Calculo da densidade do ar
            temperatura = 288.15 - 0.0065*(-pd);
            rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);
            qbar = (1/2)*rho*vt^2;
            
            wnorth = Wind(1);
            weast = Wind(2);
            wdown = Wind(3);

            Winddot = [cos(theta)*cos(psi), cos(theta)*sin(psi), -sin(theta);
                       sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), sin(phi)*cos(theta);
                       cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi), cos(phi)*cos(theta)]*Winddot;
            
            g = 9.81;

            F = [
                    cos(psi)*cos(theta)*ur + (-sin(psi)*cos(phi) + cos(psi)*sin(theta)*sin(phi))*vr + (sin(psi)*sin(phi) + cos(psi)*sin(theta)*cos(phi))*wr + wnorth;
                    sin(psi)*cos(theta)*ur + (cos(psi)*cos(phi) + sin(psi)*sin(theta)*sin(phi))*vr + (-cos(psi)*sin(phi) + sin(psi)*sin(theta)*cos(phi))*wr + weast;
                    -sin(theta)*ur + cos(theta)*sin(phi)*vr + cos(theta)*cos(phi)*wr + wdown;
                    -q*wr + r*vr - g*sin(theta) - Winddot(1);
                    p*wr - r*ur + g*sin(phi)*cos(theta) - Winddot(2);
                    -p*vr + q*ur + g*cos(phi)*cos(theta) - Winddot(3);
                    p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r;
                    cos(phi)*q - sin(phi)*r;
                    sin(phi)*q/cos(theta) + cos(phi)*r/cos(theta);
                    p*q*Aero.Jxz*(Aero.Jx - Aero.Jy + Aero.Jz)/(Aero.Jx*Aero.Jz - Aero.Jxz^2) - q*r*(Aero.Jz*(Aero.Jz - Aero.Jy) + Aero.Jxz^2)/(Aero.Jx*Aero.Jz - Aero.Jxz^2);
                    (Aero.Jz - Aero.Jx)*p*r/Aero.Jy - Aero.Jxz*(p^2 - r^2)/Aero.Jy;
                    p*q*((Aero.Jx - Aero.Jy)*Aero.Jx + Aero.Jxz^2)/(Aero.Jx*Aero.Jz - Aero.Jxz^2) - q*r*Aero.Jxz*(Aero.Jx - Aero.Jy + Aero.Jz)/(Aero.Jx*Aero.Jz - Aero.Jxz^2)
                ];
            
            G = [
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [1/Aero.uavMass, 0, 0, 0, 0, 0];
                [0, 1/Aero.uavMass, 0, 0, 0, 0];
                [0, 0, 1/Aero.uavMass, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, Aero.Jz/(Aero.Jx*Aero.Jz - Aero.Jxz^2), 0, Aero.Jxz/(Aero.Jx*Aero.Jz - Aero.Jxz^2)];
                [0, 0, 0, 0, 1/Aero.Jy, 0];
                [0, 0, 0, Aero.Jxz/(Aero.Jx*Aero.Jz - Aero.Jxz^2), 0, Aero.Jx/(Aero.Jx*Aero.Jz - Aero.Jxz^2)]
                ];
            
            %Interpolação do throttle
            throttle = obj.Controls(4);
            Throttle_y = obj.Coefficients.Thrust*(1+tanh([-3:3]));
            Throttle_x = (1/6)*(3+([-3:3]));
            Throttle = interp1(Throttle_x, Throttle_y, throttle, 'linear', 'extrap');
            
            %Matrizes de forças
            Coeffs_C = obj.coeffs_C(qbar, alpha, beta, vt, p, q, r, Throttle);
            Coeffs_M = obj.coeffs_M(qbar, alpha, beta, 0);
            
            U = Coeffs_C + Coeffs_M*controls;
            xdot = F + G*U;
            
            aux = cell2mat(obj.States);
            aux = aux + xdot*dt;
            obj.States = num2cell(aux);
        end
    end
end

