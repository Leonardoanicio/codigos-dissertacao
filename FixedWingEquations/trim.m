function [UAV, xdot] = trim(UAV, Wind, Winddot, dt)
    warning('off')
    
    states = cell2mat(UAV.States);
    controls = UAV.Controls;
    
    Q = diag([0 0 0.1 1 1 1 1 1 1 1 1 1]);
    
    %Upper bounds (controls + x < ub)
    ub = UAV.ControlBounds(:, 1) - controls;
    
    %Lower bounds (controls + x > lb)
    lb = UAV.ControlBounds(:, 2) - controls;
    
    %States lower bounds (u + x > 20) (theta + x > -pi/3)
    slb = -ones(12, 1)*inf;
    slb(1:3) = 0;
    slb(4:6) = -2;
    slb(8) = -pi/3 - states(8);
    slb(9) = 0.15;
    sub = ones(12, 1)*inf;
    sub(1:3) = 0;
    sub(4:6) = 2;
    sub(8) = pi/3 - states(8);
    sub(9) = 0.15;
    
    options = optimoptions('fmincon', 'MaxFunEvals', 1e4, 'TolCon', 1e-6, 'Display', 'notify');
    
    sizeU = size(UAV.ControlBounds, 1);
    x = fmincon(@(x)obj(x, states, controls, Q, UAV, Wind, Winddot, dt), zeros(12 + sizeU, 1), [], [], [], [],...
                            [slb; lb], [sub; ub], @(x)nonlcon(x, states, controls, UAV, Wind, Winddot, dt), options);
                        
    states = states + x(1:12);
    controls = controls + x(13:end);
    
    psi = states(9);
    theta = states(8);
    phi = states(7);
    R = [
            cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
            sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
            -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
        ];
    wind = R'*Wind;
    states(4:6) = states(4:6) - wind;
    
    UAV.States = num2cell(states);
    UAV.Controls = controls;
    [~, xdot] = UAV.EOM(dt, Wind, Winddot); 
    
    %Reinicia
    UAV.States = num2cell(states);
    UAV.Controls = controls;
end

function [c, ceq] = nonlcon(x, states, controls, UAV, Wind, Winddot, dt) 
    states = states + x(1:12);
    controls = controls + x(13:end);

    c = [];
    
    psi = states(9);
    theta = states(8);
    phi = states(7);
    R = [
            cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
            sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
            -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
        ];
    wind = R'*Wind;
    states(4:6) = states(4:6) - wind;
    
    UAV.States = num2cell(states);
    UAV.Controls = controls;
    [~, xdot] = UAV.EOM(dt, Wind, Winddot); 
    
    ceq = [];
    %Estados = 0
    ceq = [states(7); states(10:12)];
    %Derivadas = 0
    ceq = [ceq; xdot(4:12);];
end

function [fval] = obj(x, states, controls, Q, UAV, Wind, Winddot, dt)
    states = states + x(1:12);
    controls = controls + x(13:end);
    
    psi = states(9);
    theta = states(8);
    phi = states(7);
    R = [
            cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
            sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
            -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
        ];
    wind = R'*Wind;
    states(4:6) = states(4:6) - wind;
    
    UAV.States = num2cell(states);
    UAV.Controls = controls;
    [~, xdot] = UAV.EOM(dt, Wind, Winddot);   
    
    fval = xdot'*Q*xdot;
end