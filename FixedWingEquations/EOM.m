function [xdot] = EOM(states, U, UAV, Wind, Winddot)
    [pn, pe, h, ur, vr, wr, phi, theta, psi, p, q, r] = deal(states{:});
    
    wnorth = Wind(1);
    weast = Wind(2);
    wdown = Wind(3);
    
    
    R = [
            cos(psi) * cos(theta)   -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi)     sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
            sin(psi) * cos(theta)   cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi)      -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
            -sin(theta)             cos(theta) * sin(phi)                                       cos(theta) * cos(phi);
        ];
    Winddot = R'*Winddot;
    
    g = 9.81;
   
    F = [
        cos(psi)*cos(theta)*ur + (-sin(psi)*cos(phi) + cos(psi)*sin(theta)*sin(phi))*vr + (sin(psi)*sin(phi) + cos(psi)*sin(theta)*cos(phi))*wr + wnorth;
        sin(psi)*cos(theta)*ur + (cos(psi)*cos(phi) + sin(psi)*sin(theta)*sin(phi))*vr + (-cos(psi)*sin(phi) + sin(psi)*sin(theta)*cos(phi))*wr + weast;
        -sin(theta)*ur + cos(theta)*sin(phi)*vr + cos(theta)*cos(phi)*wr + wdown;
        -q*wr + r*vr - g*sin(theta) - Winddot(1);
        p*wr - r*ur + g*sin(phi)*cos(theta) - Winddot(2);
        -p*vr + q*ur + g*cos(phi)*cos(theta) - Winddot(3);
        p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r;
        cos(phi)*q - sin(phi)*r;
        sin(phi)*q/cos(theta) + cos(phi)*r/cos(theta);
        p*q*UAV.Jxz*(UAV.Jx - UAV.Jy + UAV.Jz)/(UAV.Jx*UAV.Jz - UAV.Jxz^2) - q*r*(UAV.Jz*(UAV.Jz - UAV.Jy) + UAV.Jxz^2)/(UAV.Jx*UAV.Jz - UAV.Jxz^2);
        (UAV.Jz - UAV.Jx)*p*r/UAV.Jy - UAV.Jxz*(p^2 - r^2)/UAV.Jy;
        p*q*((UAV.Jx - UAV.Jy)*UAV.Jx + UAV.Jxz^2)/(UAV.Jx*UAV.Jz - UAV.Jxz^2) - q*r*UAV.Jxz*(UAV.Jx - UAV.Jy + UAV.Jz)/(UAV.Jx*UAV.Jz - UAV.Jxz^2)
        ];
    
    G = [
        [0, 0, 0, 0, 0, 0];
        [0, 0, 0, 0, 0, 0];
        [0, 0, 0, 0, 0, 0];
        [1/UAV.uavMass, 0, 0, 0, 0, 0];
        [0, 1/UAV.uavMass, 0, 0, 0, 0];
        [0, 0, 1/UAV.uavMass, 0, 0, 0];
        [0, 0, 0, 0, 0, 0];
        [0, 0, 0, 0, 0, 0];
        [0, 0, 0, 0, 0, 0];
        [0, 0, 0, UAV.Jz/(UAV.Jx*UAV.Jz - UAV.Jxz^2), 0, UAV.Jxz/(UAV.Jx*UAV.Jz - UAV.Jxz^2)];
        [0, 0, 0, 0, 1/UAV.Jy, 0];
        [0, 0, 0, UAV.Jxz/(UAV.Jx*UAV.Jz - UAV.Jxz^2), 0, UAV.Jx/(UAV.Jx*UAV.Jz - UAV.Jxz^2)]
        ];
    
    xdot = F + G*U;
end