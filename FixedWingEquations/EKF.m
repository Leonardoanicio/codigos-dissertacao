function [EKF_P, EKF_States, EKF_derivatives, Gyro, Accel, GPS] = EKF(EKF_P, EKF_States, Gyro, Accel, GPS, oldGyro, oldAccel, oldGPS,...
                                                                    EKF_R, EKF_Q, correction, Wind, Winddot)
    
    g = 9.81;

    gyroAlpha = 0.75; %Passa baixa
    accelAlpha = 0.75;
    gpsAlpha = 0.95;
    accelSigma = 0.0025*g;
    gyroSigma = 0.0026;
    gpsSigma = 0.05;
    baroSigma = 0.2;
    
    EKFCell = num2cell(EKF_States);
    [pn, pe, pd, ur, vr, wr, phi, theta, psi] = EKFCell{:};
    Winddot = zeros(3, 1);
    dwn = Winddot(1);
    dwe = Winddot(2);
    dwd = Winddot(3);
    
    if ~correction
        %Passo de predição
        prediction_dt = 0.01;

        %Inputs do gyro -> p, q, r
        gyroNoise = random('Normal', 0, gyroSigma, 3, 1);
        Gyro = gyroAlpha*(Gyro + gyroNoise) + (1 - gyroAlpha)*oldGyro;
        p = Gyro(1); q = Gyro(2); r = Gyro(3);

        %Inputs do accel -> f/mass
        accelNoise = random('Normal', 0, accelSigma, 3, 1);
        Accel = accelAlpha*(Accel + accelNoise) + (1 - accelAlpha)*oldAccel;
        acc_x = Accel(1); acc_y = Accel(2); acc_z = Accel(3);

        %x -> pn, pe, pd, ur, vr, wr, phi, theta, psi

        %Body-NED
        R = [
                cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
                sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
                -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
            ];
        
        %NED
        Fxu1 = R*[ur; vr; wr] + Wind;
        
        %UrVrWr
        Fxu2 = [
                r*vr - q*wr - g*sin(theta) + acc_x;
                p*wr - r*ur + g*sin(phi)*cos(theta) + acc_y;
                q*ur - p*vr + g*cos(phi)*cos(theta) + acc_z;
                ] - R'*Winddot;
        %EULER
        Fxu3 = [
                p + tan(theta)*(q*sin(phi) + r*cos(phi));
                q*cos(phi) - r*sin(phi);
                (q*sin(phi) + r*cos(phi))/cos(theta);
                ];

        Fxu = [Fxu1; Fxu2; Fxu3];
        
        dF1dx = [    
                    [                                                                                                                                   0,                                                                                                                                   0,                                                              0]
                    [                                                                                                                                   0,                                                                                                                                   0,                                                              0]
                    [                                                                                                                                   0,                                                                                                                                   0,                                                              0]
                    [                                                                                                                 cos(psi)*cos(theta),                                                                                                                 cos(theta)*sin(psi),                                                    -sin(theta)]
                    [                                                                                    cos(psi)*sin(phi)*sin(theta) - cos(phi)*sin(psi),                                                                                    cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta),                                            cos(theta)*sin(phi)]
                    [                                                                                    sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta),                                                                                    cos(phi)*sin(psi)*sin(theta) - cos(psi)*sin(phi),                                            cos(phi)*cos(theta)]
                    [                         vr*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)) + wr*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)),                       - vr*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)) - wr*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)),                  vr*cos(phi)*cos(theta) - wr*cos(theta)*sin(phi)]
                    [                                             wr*cos(phi)*cos(psi)*cos(theta) - ur*cos(psi)*sin(theta) + vr*cos(psi)*cos(theta)*sin(phi),                                             wr*cos(phi)*cos(theta)*sin(psi) - ur*sin(psi)*sin(theta) + vr*cos(theta)*sin(phi)*sin(psi), - ur*cos(theta) - wr*cos(phi)*sin(theta) - vr*sin(phi)*sin(theta)]
                    [ wr*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)) - vr*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)) - ur*cos(theta)*sin(psi), wr*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)) - vr*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)) + ur*cos(psi)*cos(theta),                                                              0]
                 ];

        dF2dx = [
                    [             0,                      0,                      0]
                    [             0,                      0,                      0]
                    [             0,                      0,                      0]
                    [             0,                     -r,                      q]
                    [             r,                      0,                     -p]
                    [            -q,                      p,                      0]
                    [ 0, dwe*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)) - dwn*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)) - dwd*cos(phi)*cos(theta) + g*cos(phi)*cos(theta), dwe*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)) - dwn*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)) + dwd*cos(theta)*sin(phi) - g*cos(theta)*sin(phi)]
                    [ dwd*cos(theta) - g*cos(theta) + dwn*cos(psi)*sin(theta) + dwe*sin(psi)*sin(theta), dwd*sin(phi)*sin(theta) - g*sin(phi)*sin(theta) - dwn*cos(psi)*cos(theta)*sin(phi) - dwe*cos(theta)*sin(phi)*sin(psi), dwd*cos(phi)*sin(theta) - g*cos(phi)*sin(theta) - dwn*cos(phi)*cos(psi)*cos(theta) - dwe*cos(phi)*cos(theta)*sin(psi)]
                    [ dwn*cos(theta)*sin(psi) - dwe*cos(psi)*cos(theta), dwe*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)) + dwn*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)), - dwe*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)) - dwn*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta))]
                ];

        dF3dx = [
                    [                                            0,                         0,                                                   0]
                    [                                            0,                         0,                                                   0]
                    [                                            0,                         0,                                                   0]
                    [                                            0,                         0,                                                   0]
                    [                                            0,                         0,                                                   0]
                    [                                            0,                         0,                                                   0]
                    [         tan(theta)*(q*cos(phi) - r*sin(phi)), - r*cos(phi) - q*sin(phi),                (q*cos(phi) - r*sin(phi))/cos(theta)]
                    [ (tan(theta)^2 + 1)*(r*cos(phi) + q*sin(phi)),                         0, (sin(theta)*(r*cos(phi) + q*sin(phi)))/cos(theta)^2]
                    [                                            0,                         0,                                                   0]
            ];

        dFdx = [dF1dx'; dF2dx'; dF3dx'];

        EKF_A = dFdx;
        EKF_derivatives = Fxu;
        EKF_States = EKF_States + EKF_derivatives*prediction_dt;
        EKF_P = EKF_P + (EKF_A*EKF_P + EKF_P*EKF_A' + EKF_Q)*prediction_dt;
    else   
        %Passo de correção
        %Saida dada pelo GPS + barômetro
        gpsNoise = random('Normal', 0, gpsSigma, 2, 1);
        baroNoise = random('Normal', 0, baroSigma, 1, 1);
        GPS = gpsAlpha*(GPS + [gpsNoise; baroNoise]) + (1 - gpsAlpha)*oldGPS;
        
        hxu = EKF_States(1:3);
        dhdx = [eye(3), zeros(3, 6)];
        
        C = dhdx;
        L = EKF_P*C'/(EKF_R + C*EKF_P*C');
        EKF_P = (eye(size(L*C)) - L*C)*EKF_P;
        EKF_States = EKF_States + L*(GPS - hxu);
        EKF_derivatives =[];
    end
end

