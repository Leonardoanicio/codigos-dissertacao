classdef Rascal
    properties
        Coefficients
        States
        Controls
    end
    
    methods
        function obj = Rascal(States, Controls)
            obj.Coefficients = RascalLoad();
            obj.States = States;
            obj.Controls = Controls;
        end
        
        function C_6x1 = coeffs_C(obj, alpha, beta, vt, p, q, r, alphadot, rho)
            UAV = obj.Coefficients;
            qbar = 0.5*rho*vt^2;
            
            CL = (-14.3018*alpha^3 + 0.4291*alpha^2  + 5.6579*alpha + 0.2500);
            L = UAV.Sw*qbar*CL;
            D = UAV.Sw*qbar*(0.6005*alpha^2 + 0.0197 + CL*CL*0.04 + 0.4946*beta^2 + 0.0109);
            Y = -UAV.Sw*qbar*beta;
            
            FWIND = [-D; Y; -L];
            FAERO = (rotz(rad2deg(beta))'*roty(rad2deg(alpha)))'*FWIND;

            %roll
            clb = qbar*UAV.Sw*UAV.Bw*beta*-0.1;
            clp = qbar*UAV.Sw*UAV.Bw*UAV.Bw/(2*vt)*p*-0.4;
            clr = qbar*UAV.Sw*UAV.Bw*UAV.Bw/(2*vt)*p*0.15;
            l = clb + clp + clr;
            
            %pitch
            cma = qbar*UAV.Sw*UAV.Cw*alpha*-0.5;
            cmq = qbar*UAV.Sw*UAV.Cw*UAV.Cw/(2*vt)*q*-12;
            cmad = qbar*UAV.Sw*UAV.Cw*UAV.Cw/(2*vt)*alphadot*-7;
            m = cma + cmq + cmad;
            
            %yaw
            cnb = qbar*UAV.Sw*UAV.Bw*beta*0.12;
            cnr = qbar*UAV.Sw*UAV.Bw*UAV.Bw/(2*vt)*r*-0.15;
            cni = qbar*UAV.Sw*UAV.Bw*0.0007;
            n = cnb + cnr + cni;

            MAERO = [l; m; n];
            r_cross = (UAV.r_aero_abc - UAV.r_cg_abc)';

            M2 = cross(r_cross, FAERO);

            FORCES = FAERO;
            MOMENTS = MAERO + M2;
            C_6x1 = vertcat(FORCES, MOMENTS);
        end
        
        function M_6x4 = coeffs_M(obj, alpha, beta, vt, rho)
            UAV = obj.Coefficients;
            qbar = 0.5*rho*vt^2;
            
            clde = qbar*UAV.Sw*0.2;
            L = [0 clde 0];
            
            cdde = qbar*UAV.Sw*0.03/0.3;
            D = [0 cdde 0];
            
            Y = [0 0 0];
            
            FWIND = [-D; Y; -L];
            FAERO = (rotz(rad2deg(beta))'*roty(rad2deg(alpha)))'*FWIND;
            
            mach = 0.002938669957977*vt;
            clda = qbar*UAV.Sw*UAV.Bw*(-0.0365*mach + 0.1300);
            cldr = qbar*UAV.Sw*UAV.Bw*0.01;
            l = [clda 0 cldr];
            
            cmde = qbar*UAV.Sw*UAV.Cw*(0.1125*mach - 0.5000);
            m = [0 cmde 0];
            
            cndr = qbar*UAV.Sw*UAV.Bw*-0.05;
            cnda = qbar*UAV.Sw*UAV.Bw*-0.03;
            n = [cnda 0 cndr];
            
            MAERO = [l; m; n];
            
            M_6x4 = [[FAERO; MAERO], [UAV.F_T; 0; 0; 0; 0; 0]];
        end
        
        function [Coeffs_M, Coeffs_C, Coeffs_P, alpha] = getMatrices(obj, alphaold)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);

            alphadot = (alpha - alphaold)/0.01;

            %Calculo da densidade do ar
            temperatura = 288.15 - 0.0065*(-pd);
            rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

            %Matrizes de forças
            Coeffs_C = coeffs_C(obj, alpha, beta, vt, p, q, r, alphadot, rho);
            Coeffs_M = coeffs_M(obj, alpha, beta, vt, rho);
            Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
        end
    end
end

