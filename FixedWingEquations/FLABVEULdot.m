function [Alpha, Beta] = FLABVEULdot(states, C, UAV)    
    [pn, pe, h, ur, vr, wr, phi, theta, psi, p, q, r] = deal(states{:});

    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    g = 9.81;
    
    Beta = [
            [-UAV.uavMass * wr -UAV.uavMass * sqrt((ur ^ 2 + wr ^ 2) / (ur ^ 2 + vr ^ 2 + wr ^ 2)) * sqrt(ur ^ 2 + vr ^ 2 + wr ^ 2) * vr * ur / (ur ^ 2 + wr ^ 2) UAV.uavMass * (ur ^ 2 + vr ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) * ur 0 0 0];
            [0 UAV.uavMass * sqrt((ur ^ 2 + wr ^ 2) / (ur ^ 2 + vr ^ 2 + wr ^ 2)) * sqrt(ur ^ 2 + vr ^ 2 + wr ^ 2) UAV.uavMass * (ur ^ 2 + vr ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) * vr 0 0 0];
            [UAV.uavMass * ur -UAV.uavMass * sqrt((ur ^ 2 + wr ^ 2) / (ur ^ 2 + vr ^ 2 + wr ^ 2)) * sqrt(ur ^ 2 + vr ^ 2 + wr ^ 2) * wr / (ur ^ 2 + wr ^ 2) * vr UAV.uavMass * (ur ^ 2 + vr ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) * wr 0 0 0];
            [0 0 0 UAV.Jx sin(phi) * UAV.Jxz -UAV.Jxz * cos(phi) * cos(theta) - UAV.Jx * sin(theta)];
            [0 0 0 0 UAV.Jy * cos(phi) UAV.Jy * cos(theta) * sin(phi)];
            [0 0 0 -UAV.Jxz -sin(phi) * UAV.Jz UAV.Jz * cos(phi) * cos(theta) + UAV.Jxz * sin(theta)];
        ];


    Alpha = [
        g*UAV.uavMass*sin(theta) + q*UAV.uavMass*wr - r*UAV.uavMass*vr - fx_ex;
        -sin(phi)*cos(theta)*g*UAV.uavMass - p*UAV.uavMass*wr + r*UAV.uavMass*ur - fy_ex;
        -cos(phi)*cos(theta)*g*UAV.uavMass + p*UAV.uavMass*vr - q*UAV.uavMass*ur - fz_ex;
        ((-0.2e1 * UAV.Jx * cos(phi) ^ 2 * q * r - sin(phi) * UAV.Jx * (q - r) * (q + r) * cos(phi) + r * (UAV.Jx - UAV.Jy + UAV.Jz) * q - l_ex) * cos(theta) + (0.2e1 * q * cos(phi) ^ 3 * r + sin(phi) * (q ^ 2 - r ^ 2) * cos(phi) ^ 2 + q ^ 2 * sin(phi)) * sin(theta) * UAV.Jxz) / cos(theta);
        ((-UAV.Jxz * r ^ 2 + p * (UAV.Jx + UAV.Jy - UAV.Jz) * r + UAV.Jxz * p ^ 2 - m_ex) * cos(theta) + sin(theta) * ((q ^ 2 - r ^ 2) * cos(phi) ^ 3 - 0.2e1 * sin(phi) * q * r * cos(phi) ^ 2 + (-q ^ 2 + 0.2e1 * r ^ 2) * cos(phi) + 0.2e1 * sin(phi) * q * r) * UAV.Jy) / cos(theta);
        ((0.2e1 * UAV.Jxz * cos(phi) ^ 2 * q * r + sin(phi) * UAV.Jxz * (q - r) * (q + r) * cos(phi) - p * (UAV.Jx - UAV.Jy + UAV.Jz) * q - n_ex) * cos(theta) - 0.2e1 * sin(theta) * (q * cos(phi) ^ 3 * r + sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) ^ 2 + q ^ 2 * sin(phi) / 0.2e1) * UAV.Jz) / cos(theta);
    ];
end