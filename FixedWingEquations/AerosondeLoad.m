function Aerosonde = AerosondeLoad()
    % ----------------- PARAMETROS UAV MODEL ----------------------------------
    % -------------------------------------------------------------------------
    % ------------------------ GERAIS------------------------------------------

    Aerosonde = struct();

    %mean chord (m)
    Aerosonde.Cw = 0.189941;

    %wing span (m)
    Aerosonde.Bw = 2.8956; % m

    %wing area (m^2)
    Aerosonde.Sw = 0.55; % m^2

    %aspect ratio
    Aerosonde.AR = (Aerosonde.Bw^2)/Aerosonde.Sw;

    % Oswald's coefficient
    Aerosonde.osw = 0.75;

    %gravidade (m/s^2)     
    Aerosonde.gravity = 9.807;

    %massa da aeronave (Kg)
    Aerosonde.uavMass = 8.5;

    %componentes de in�rcia da aeronave
    Aerosonde.Jx = 0.7795;
    Aerosonde.Jy = 1.122;
    Aerosonde.Jz = 1.752;
    Aerosonde.Jxz = 0.1211;
    Aerosonde.Jxy = 0;
    Aerosonde.Jyz = 0;

    %posi��o do c.a. no eixo estrutural (m)
    r_aero_struct =    [0.1425 0 0];

    %posi��o do c.g. no eixo estrutural (m)
    r_cg_struct =      [0.156 0 0];

    %convers�o do eixo estrutural para o ABC (m)
    Aerosonde.r_aero_abc  = r_aero_struct*[-1 0 0;0 1 0;0 0 -1];
    Aerosonde.r_cg_abc    = r_cg_struct*[-1 0 0;0 1 0;0 0 -1];  
    
    % -------------------------------------------------------------------------
    % ------------------------ SERVOS------------------------------------------
    % -------------------------------------------------------------------------
    %Limites dos sinais de controle
    angle_e = deg2rad(15); 
    angle_r = deg2rad(20);
    angle_a = deg2rad(20);

    Aerosonde.UpperBounds = [angle_a angle_e angle_r 1]';
    Aerosonde.LowerBounds = [-angle_a -angle_e -angle_r 0.01]';

    % -------------------------------------------------------------------------
    % ------------------------ AERODIN�MICOS-----------------------------------
    % -------------------------------------------------------------------------

    %---------------------------Drag Force-------------------------------------
    %coeficientes aerodin�micos de arrasto
    Aerosonde.CDbeta = 0;
    Aerosonde.CD_delta_elev = 0.0135;
    Aerosonde.CD_delta_ail = 0.0302;
    Aerosonde.CD_delta_rud = 0.0303;
    Aerosonde.CD_mind = 0.23;
    Aerosonde.CD_0 = 0.0434; %CD0a
    Aerosonde.induced_drag = 1/(pi*Aerosonde.AR*Aerosonde.osw);

    %---------------------------Side Force-------------------------------------
    %coeficientes aerodin�micos de for�a lateral
    Aerosonde.CYbeta = -0.83;
    Aerosonde.CY_delta_ail = -0.075;
    Aerosonde.CY_delta_rud = 0.1914;
    Aerosonde.CYp = 0;
    Aerosonde.CYr = 0;

    %---------------------------Lift Force-------------------------------------

    %----------------------------Asa-------------------------------------------
    %coeficientes aerodin�micos da for�a de sustenta��o
    Aerosonde.CLbeta = 5.6106;
    Aerosonde.CL_0 = 0.23; %CL0
    Aerosonde.CL_delta_elev = 0.13;
    Aerosonde.CL_alphadot =  1.9724; 
    Aerosonde.CLq = 7.9543;

    %----------------------------Empenagem-------------------------------------
    % coeficientes aerodin�micos da cauda
    Aerosonde.CLE_elev = 0;

    %--------------------------Rolling Moment----------------------------------
    % coeficientes aerodin�micos de rolagem
    Aerosonde.Clbeta = -0.13;
    Aerosonde.Cl_delta_ail = -0.1695;
    Aerosonde.Cl_delta_rud = 0.0024;
    Aerosonde.Clp = -0.5051;
    Aerosonde.Clr = 0.2519;

    %--------------------------Pitching Moment---------------------------------
    % coeficientes aerodin�micos de arfagem
    Aerosonde.Cm0 = 0.135; %Cm0
    Aerosonde.Cmbeta = -2.7397;
    Aerosonde.Cm_delta_elev = -0.9918;
    Aerosonde.Cm_alphadot = -10.3796;
    Aerosonde.Cmq = -38.2067;

    %--------------------------Yawing Moment-----------------------------------
    %coeficientes aerodin�micos de guinada
    Aerosonde.Cnbeta = 0.0726;
    Aerosonde.Cn_delta_ail = 0.0108;
    Aerosonde.Cn_delta_rud = -0.0693;
    Aerosonde.Cnp = -0.069;
    Aerosonde.Cnr = -0.0946;

    % -------------------------------------------------------------------------
    % ------------------------ MOTOR ------------------------------------------
    % -------------------------------------------------------------------------
    Aerosonde.F_T = 19; %N
end