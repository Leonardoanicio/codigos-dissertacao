close all;
clc;

Aero = AerosondeBeardLoad();

L = [];
for alpha=-1:0.01:1
    sigma = (1 + exp(-Aero.M*(alpha - Aero.alpha0)) + exp(Aero.M*(alpha + Aero.alpha0)))/...
            ((1 + exp(-Aero.M*(alpha - Aero.alpha0)))*(1 + exp(Aero.M*(alpha + Aero.alpha0))));
    CL = (1 - sigma)*(Aero.CL0 + Aero.CLalpha*alpha) + sigma*(2*sign(alpha)*sin(alpha)^2*cos(alpha));
    L = [L CL];
end

alpha = -1:0.01:1;
hold;
plot(alpha, L, 'LineWidth', 2);
xlabel('Angle-of-attack (rad)');
ylabel('Lift coefficient');
title('Lift curve for the Aerosonde UAV');
grid
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
set(findall(gcf,'-property','FontSize'),'FontSize',12);

vstall = sqrt(9.81*Aero.uavMass/(0.5*Aero.rho*max(L)*Aero.S));
alpha0 = find(diff(sign(L)) ~= 0);
alpha0 = alpha(alpha0 + 1);

plot([alpha0 alpha0], [-1.5 2], 'k--', 'LineWidth', 2)
plot([alpha(L == max(L)) alpha(L == max(L))], [-1.5 2], 'k--', 'LineWidth', 2)
% legend('Lift coefficient', 'Angle-of-attack limits');
