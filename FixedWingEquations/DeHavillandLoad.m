function [controls, states] = DeHavillandLoad()
    load('op_trim.mat');
    opstates = getstatestruct(op_trim);
    opinputs = getinputstruct(op_trim);

    controls = [
        opinputs.signals(1).values;
        opinputs.signals(2).values;
        opinputs.signals(3).values;
        opinputs.signals(4).values;
        ];

    xme_0 = [
        opstates.signals(10).values; %E
        opstates.signals(11).values; %N
        opstates.signals(12).values; %D
        ];

    pm_0 = [
        opstates.signals(4).values; %p
        opstates.signals(5).values; %q
        opstates.signals(6).values; %r
        ];

    Vm_0 = [
        opstates.signals(7).values; %u
        opstates.signals(8).values; %v
        opstates.signals(9).values; %w
        ];

    eul_0 = [
        opstates.signals(1).values; %phi
        opstates.signals(2).values; %theta
        opstates.signals(3).values; %psi
        ];
    
    states = [xme_0; Vm_0; eul_0; pm_0];
end