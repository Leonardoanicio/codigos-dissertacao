function Zagi = ZagiBeardLoad()
    % ----------------- PARAMETROS UAV MODEL ----------------------------------
    % -------------------------------------------------------------------------
    % ------------------------ GERAIS------------------------------------------

    Zagi = struct();
    
    Zagi.uavMass = 1.56;
    Zagi.Jx = 0.1147;
    Zagi.Jy = 0.0576;
    Zagi.Jz = 0.1712;
    Zagi.Jxz = 0.0015;
    Zagi.S = 0.2589;
    Zagi.b = 1.4224;
    Zagi.c = 0.3302;
    Zagi.Sprop = 0.0314;
    Zagi.rho = 1.2682;
    Zagi.Kmotor = 20;
    Zagi.Ktp = 0;
    Zagi.Komega = 0;
    Zagi.e = 0.9;
    Zagi.CL0 = 0.09167;
    Zagi.CD0 = 0.01631;
    Zagi.Cm0 = -0.02338;
    Zagi.CLalpha = 3.5016;
    Zagi.CDalpha = 0.2108;
    Zagi.Cmalpha = -0.5675;
    Zagi.CLq = 2.8932;
    Zagi.CDq = 0;
    Zagi.Cmq = -1.399;
    Zagi.CLde = 0.2724;
    Zagi.CDde = 0.3045;
    Zagi.Cmde = -0.3254;
    Zagi.Cprop = 1;
    Zagi.M = 50;
    Zagi.alpha0 = 0.4712;
    Zagi.epsilon = 0.1592;
    Zagi.CDp = 0.0254;
    Zagi.CY0 = 0;
    Zagi.Cl0 = 0;
    Zagi.Cn0 = 0;
    Zagi.CYbeta = -0.07359;
    Zagi.Clbeta = -0.02854;
    Zagi.Cnbeta = -0.0004;
    Zagi.CYp = 0;
    Zagi.Clp = -0.3209;
    Zagi.Cnp = -0.01297;
    Zagi.CYr = 0;
    Zagi.Clr = 0.03066;
    Zagi.Cnr = -0.00434;
    Zagi.CYda = 0;
    Zagi.Clda = 0.1682;
    Zagi.Cnda = 0.1682;
    Zagi.AR = Zagi.b^2/Zagi.S;
end