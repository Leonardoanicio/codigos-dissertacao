classdef Aerosonde
    properties
        Coefficients
        States
        Controls
    end
    
    methods
        function obj = Aerosonde(States, Controls)
            obj.Coefficients = AerosondeLoad();
            obj.States = States;
            obj.Controls = Controls;
        end
        
        function C_6x1 = coeffs_C(obj, alpha, beta, vt, p, q, r, alphadot, rho)
            UAV = obj.Coefficients;
            L = (UAV.CLbeta*alpha + UAV.CL_0 + (UAV.CL_alphadot*alphadot + UAV.CLq*q)*UAV.Cw/(2*vt));
            CL = L;
            D = (UAV.CDbeta*beta + UAV.CD_0 + UAV.induced_drag*(CL - UAV.CD_mind)^2);
            Y = (UAV.CYbeta*beta + (UAV.CYp*p + UAV.CYr*r)*UAV.Bw/(2*vt));

            FWIND = 0.5*rho*vt^2*UAV.Sw*[-D; Y; -L];
            FAERO = (rotz(rad2deg(beta))'*roty(rad2deg(alpha)))'*FWIND;

            l = (UAV.Clbeta*beta + (UAV.Clp*p + UAV.Clr*r)*UAV.Bw/(2*vt))*UAV.Bw;
            m = (UAV.Cm0 + UAV.Cmbeta*alpha + (UAV.Cm_alphadot*alphadot + UAV.Cmq*q)*UAV.Cw/(2*vt))*UAV.Cw;
            n = (UAV.Cnbeta*beta + (UAV.Cnp*p + UAV.Cnr*r)*UAV.Bw/(2*vt))*UAV.Bw;

            MAERO = 0.5*rho*vt^2*UAV.Sw*[l; m; n];
            r_cross = (UAV.r_aero_abc - UAV.r_cg_abc)';

            M2 = cross(r_cross, FAERO);

            FORCES = FAERO;
            MOMENTS = MAERO + M2;
            C_6x1 = vertcat(FORCES, MOMENTS);
        end
        
        function M_6x4 = coeffs_M(obj, alpha, beta, vt, ail, elev, rud, rho)
            UAV = obj.Coefficients;
            selev = sign(elev);
            if selev == 0
                selev = 1;
            end

            sail= sign(ail);
            if sail == 0
                sail = 1;
            end

            srud = sign(rud);
            if srud == 0
                srud = 1;
            end

            CD_delta_ail_local = UAV.CD_delta_ail*sail;
            CD_delta_elev_local = UAV.CD_delta_elev*selev;
            CD_delta_rud_local = UAV.CD_delta_rud*srud;

            Fail = [
                    -cos(alpha)*(cos(beta)*CD_delta_ail_local + sin(beta)*UAV.CY_delta_ail);
                    (-sin(beta)*CD_delta_ail_local + cos(beta)*UAV.CY_delta_ail);
                    -sin(alpha)*(cos(beta)*CD_delta_ail_local + sin(beta)*UAV.CY_delta_ail)
                    ];
            Felev = [
                    (-cos(beta)*cos(alpha)*CD_delta_elev_local + sin(alpha)*UAV.CL_delta_elev);
                    -sin(beta)*CD_delta_elev_local;
                    -(cos(beta)*sin(alpha)*CD_delta_elev_local + cos(alpha)*UAV.CL_delta_elev)
                    ];
            Frud  = [
                    -cos(alpha)*(cos(beta)*CD_delta_rud_local + sin(beta)*UAV.CY_delta_rud);
                    (-sin(beta)*CD_delta_rud_local + cos(beta)*UAV.CY_delta_rud);
                    -sin(alpha)*(cos(beta)*CD_delta_rud_local + sin(beta)*UAV.CY_delta_rud)
                    ];
            Fthr = [
                    UAV.F_T;
                    0;
                    0
                    ];

            Forces = horzcat(0.5*rho*vt^2*UAV.Sw*Fail, 0.5*rho*vt^2*UAV.Sw*Felev,...
                0.5*rho*vt^2*UAV.Sw*Frud, Fthr);

            Mail = [
                    UAV.Bw* UAV.Cl_delta_ail;
                    0.;
                    UAV.Bw* UAV.Cn_delta_ail
                    ];
            Melev = [
                    0.;
                    UAV.Cw* UAV.Cm_delta_elev;
                    0.
                    ];
            Mrud = [
                    UAV.Bw* UAV.Cl_delta_rud;
                    0.;
                    UAV.Bw* UAV.Cn_delta_rud
                    ];
            Mthr = [0;
                    0;
                    0;
                    ];
            Moments = horzcat(0.5*rho*vt^2*UAV.Sw*Mail, 0.5*rho*vt^2*UAV.Sw*Melev,...
                0.5*rho*vt^2*UAV.Sw*Mrud, Mthr);

            M_6x4 = vertcat(Forces, Moments);
        end
        
        function [Coeffs_M, Coeffs_C, Coeffs_P, alpha] = getMatrices(obj, alphaold)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);

            alphadot = (alpha - alphaold)/0.01;

            %Controles atuais
            ail = obj.Controls(1);
            elev = obj.Controls(2);
            rud = obj.Controls(3);

            %Calculo da densidade do ar
            temperatura = 288.15 - 0.0065*(-pd);
            rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

            %Matrizes de forças
            Coeffs_C = coeffs_C(obj, alpha, beta, vt, p, q, r, alphadot, rho);
            Coeffs_M = coeffs_M(obj, alpha, beta, vt, ail, elev, rud, rho);
            Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
        end
    end
end

