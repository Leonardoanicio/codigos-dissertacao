function Rascal = RascalLoad()
    % ----------------- PARAMETROS UAV MODEL ----------------------------------
    % -------------------------------------------------------------------------
    % ------------------------ GERAIS------------------------------------------

    Rascal = struct();

    %mean chord (m)
    Rascal.Cw = 1.15*0.3048;

    %wing span (m)
    Rascal.Bw = 9.17*0.3048; % m

    %wing area (m^2)
    Rascal.Sw = 10.57*0.3048; % m^2

    %aspect ratio
    Rascal.AR = (Rascal.Bw^2)/Rascal.Sw;

    % Oswald's coefficient
    Rascal.osw = 0.75;

    %gravidade (m/s^2)     
    Rascal.gravity = 9.807;

    %massa da aeronave (Kg)
    Rascal.uavMass = 6;

    %componentes de in�rcia da aeronave
    Rascal.Jx = 1.95*1.35;
    Rascal.Jy = 1.55*1.35;
    Rascal.Jz = 1.91*1.35;
    Rascal.Jxz = 0;
    Rascal.Jxy = 0;
    Rascal.Jyz = 0;

    %posi��o do c.a. no eixo estrutural (m)
    r_aero_struct =    [37.4 0 0]*0.0254;

    %posi��o do c.g. no eixo estrutural (m)
    r_cg_struct =      [36.4 0 4]*0.0254;

    %convers�o do eixo estrutural para o ABC (m)
    Rascal.r_aero_abc  = r_aero_struct*[-1 0 0;0 1 0;0 0 -1];
    Rascal.r_cg_abc    = r_cg_struct*[-1 0 0;0 1 0;0 0 -1];  
    
    % -------------------------------------------------------------------------
    % ------------------------ SERVOS------------------------------------------
    % -------------------------------------------------------------------------
    %Limites dos sinais de controle
    Rascal.UpperBounds = [0.35 0.3 0.35 1]';
    Rascal.LowerBounds = [-0.35 -0.3 -0.35 0.01]';


    % -------------------------------------------------------------------------
    % ------------------------ MOTOR ------------------------------------------
    % -------------------------------------------------------------------------
    Rascal.F_T = 49; %N
end