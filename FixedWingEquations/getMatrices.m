function [Coeffs_M, Coeffs_C, Coeffs_P, alpha] = getMatrices(states, controls, UAV, alphaold)
    [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(states{:});
        
    %Alpha, Beta, Va
    alpha = atan2(wr,ur);
    vt = norm([ur,vr,wr]);
    beta = asin(vr/vt);

    alphadot = (alpha - alphaold)/0.01;

    %Controles atuais
    ail = controls(1);
    elev = controls(2);
    rud = controls(3);

    %Calculo da densidade do ar
    temperatura = 288.15 - 0.0065*(-pd);
    rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

    %Matrizes de forças
    Coeffs_C = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV);
    Coeffs_M = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV);
    Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
end