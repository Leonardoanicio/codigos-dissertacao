function [Alpha, Beta] = FLNEDdotEULdot(states, C, UAV)
    [pn, pe, h, ur, vr, wr, phi, theta, psi, p, q, r] = deal(states{:});
    
    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
  
    
    g = 9.81;
    
    Beta = [
            [UAV.uavMass * cos(psi) * cos(theta) UAV.uavMass * sin(psi) * cos(theta) -UAV.uavMass * sin(theta) 0 0 0];
            [-UAV.uavMass * (sin(psi) * cos(phi) - cos(psi) * sin(theta) * sin(phi)) UAV.uavMass * (cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi)) UAV.uavMass * cos(theta) * sin(phi) 0 0 0];
            [UAV.uavMass * (sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi)) UAV.uavMass * (-cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi)) UAV.uavMass * cos(theta) * cos(phi) 0 0 0];
            [0 0 0 UAV.Jx sin(phi) * UAV.Jxz -UAV.Jxz * cos(phi) * cos(theta) - sin(theta) * UAV.Jx];
            [0 0 0 0 UAV.Jy * cos(phi) cos(theta) * UAV.Jy * sin(phi)];
            [0 0 0 -UAV.Jxz -sin(phi) * UAV.Jz UAV.Jz * cos(phi) * cos(theta) + sin(theta) * UAV.Jxz];
        ];
    
    Alpha = [ 
                sin(theta)*g*UAV.uavMass - fx_ex;
              -sin(phi)*cos(theta)*g*UAV.uavMass - fy_ex;
              -cos(phi)*cos(theta)*g*UAV.uavMass - fz_ex;
              ((-0.2e1 * UAV.Jx * cos(phi) ^ 2 * q * r - sin(phi) * UAV.Jx * (q - r) * (q + r) * cos(phi) + r * (UAV.Jx - UAV.Jy + UAV.Jz) * q - l_ex) * cos(theta) + (0.2e1 * q * cos(phi) ^ 3 * r + sin(phi) * (q ^ 2 - r ^ 2) * cos(phi) ^ 2 + q ^ 2 * sin(phi)) * UAV.Jxz * sin(theta)) / cos(theta);
              ((-UAV.Jxz * r ^ 2 + p * (UAV.Jx + UAV.Jy - UAV.Jz) * r + UAV.Jxz * p ^ 2 - m_ex) * cos(theta) + UAV.Jy * sin(theta) * ((q ^ 2 - r ^ 2) * cos(phi) ^ 3 - 0.2e1 * sin(phi) * q * r * cos(phi) ^ 2 + (-q ^ 2 + 0.2e1 * r ^ 2) * cos(phi) + 0.2e1 * sin(phi) * q * r)) / cos(theta);
              ((0.2e1 * UAV.Jxz * cos(phi) ^ 2 * q * r + sin(phi) * UAV.Jxz * (q - r) * (q + r) * cos(phi) - p * (UAV.Jx - UAV.Jy + UAV.Jz) * q - n_ex) * cos(theta) - 0.2e1 * sin(theta) * UAV.Jz * (q * cos(phi) ^ 3 * r + sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) ^ 2 + q ^ 2 * sin(phi) / 0.2e1)) / cos(theta);
             ];
end