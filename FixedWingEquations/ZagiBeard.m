classdef ZagiBeard
    properties
        alphass
        Coefficients
        States
        Controls
        ControlBounds
        StateBounds
        Type
    end
    
    methods
        function obj = ZagiBeard(States, Controls)
            obj.Coefficients = ZagiBeardLoad();
            obj.States = num2cell(States);
            obj.Controls = Controls;
            obj.ControlBounds = [
                                    [0.52     0.52    1.0000];
                                    [-0.52    -0.52    0.01];
                                ]';
            obj.StateBounds = [%Alpha vt theta
                                    [0.42 100 pi/2.1];
                                    [-0.0300 8 -pi/2.1];
                                ]';
            obj.Type = 'elevon';
        end
        
        function C_6x1 = coeffs_C(obj, Aero, qbar, alpha, beta, vt, p, q, r, fx_throttle)
            sigma = (1 + exp(-Aero.M*(alpha - Aero.alpha0)) + exp(Aero.M*(alpha + Aero.alpha0)))/...
                    ((1 + exp(-Aero.M*(alpha - Aero.alpha0)))*(1 + exp(Aero.M*(alpha + Aero.alpha0))));
            CL = (1 - sigma)*(Aero.CL0 + Aero.CLalpha*alpha) + sigma*(2*sign(alpha)*sin(alpha)^2*cos(alpha));
            CD = Aero.CDp + (Aero.CL0 + Aero.CLalpha*alpha)^2/(pi*Aero.e*Aero.AR);
            
            Cxalpha = -CD*cos(alpha) + CL*sin(alpha);
            Cxq = -Aero.CDq*cos(alpha) + Aero.CLq*sin(alpha);
            Czalpha = -CD*sin(alpha) - CL*cos(alpha);
            Czq = -Aero.CDq*sin(alpha) - Aero.CLq*cos(alpha);
            
            
            fx = qbar*Aero.S*(Cxalpha + Cxq*Aero.c*q/(2*vt)) + qbar/(vt^2)*Aero.Sprop*Aero.Cprop*(Aero.Kmotor^2*fx_throttle - vt^2);
            fy = qbar*Aero.S*(Aero.CY0 + Aero.CYbeta*beta + Aero.CYp*Aero.b*p/(2*vt) + Aero.CYr*Aero.b*r/(2*vt));
            fz = qbar*Aero.S*(Czalpha + Czq*Aero.c*q/(2*vt));
            l = qbar*Aero.S*(Aero.b*(Aero.Cl0 + Aero.Clbeta*beta + Aero.Clp*Aero.b*p/(2*vt) + Aero.Clr*Aero.b*r/(2*vt)));
            m = qbar*Aero.S*(Aero.c*(Aero.Cm0 + Aero.Cmalpha*alpha + Aero.Cmq*Aero.c*q/(2*vt)));
            n = qbar*Aero.S*(Aero.b*(Aero.Cn0 + Aero.Cnbeta*beta + Aero.Cnp*Aero.b*p/(2*vt) + Aero.Cnr*Aero.b*r/(2*vt)));
            C_6x1 = [fx; fy; fz; l; m; n];
        end
        
        function M_6x4 = coeffs_M(obj, Aero, qbar, alpha, vt, fx_throttle)
            Cxde = -Aero.CDde*cos(alpha) + Aero.CLde*sin(alpha);
            Czde = -Aero.CDde*sin(alpha) - Aero.CLde*cos(alpha);
        
            fx = [0 qbar*Aero.S*Cxde qbar*Aero.Sprop*Aero.Cprop*Aero.Kmotor^2*fx_throttle/(vt^2)-...
                                            Aero.Ktp*Aero.Komega^2*fx_throttle];
            fy = [qbar*Aero.S*Aero.CYda 0 0];
            fz = [0 qbar*Aero.S*Czde 0];
            l = [qbar*Aero.S*Aero.b*Aero.Clda 0 0];
            m = [0 qbar*Aero.S*Aero.c*Aero.Cmde 0];
            n = [qbar*Aero.S*Aero.b*Aero.Cnda 0 0];
                
            M_6x4 = [fx; fy; fz; l; m; n];
        end
        
        function [Coeffs_M, Coeffs_C, Coeffs_P, alpha] = getMatrices(obj)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);

            %Matrizes de forças
            Aero = obj.Coefficients;
            qbar = (1/2)*Aero.rho*vt^2;
            
            %Lineariza throttle^2
            throttle = obj.Controls(3);
            b = -throttle^2;
            a = 2*throttle;
            
            Coeffs_C = obj.coeffs_C(Aero, qbar, alpha, beta, vt, p, q, r, b);
            Coeffs_M = obj.coeffs_M(Aero, qbar, alpha, vt, a);
            Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
        end
        
        function [obj, xdot] = EOM(obj, dt, Wind, Winddot)
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = deal(obj.States{:});
            controls = obj.Controls;
            Aero = obj.Coefficients;
            
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);
            qbar = (1/2)*Aero.rho*vt^2;
            
            wnorth = Wind(1);
            weast = Wind(2);
            wdown = Wind(3);

            Winddot = [cos(theta)*cos(psi), cos(theta)*sin(psi), -sin(theta);
                       sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), sin(phi)*cos(theta);
                       cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi), cos(phi)*cos(theta)]*Winddot;
            g = 9.81;

            F = [
                cos(psi)*cos(theta)*ur + (-sin(psi)*cos(phi) + cos(psi)*sin(theta)*sin(phi))*vr + (sin(psi)*sin(phi) + cos(psi)*sin(theta)*cos(phi))*wr + wnorth;
                sin(psi)*cos(theta)*ur + (cos(psi)*cos(phi) + sin(psi)*sin(theta)*sin(phi))*vr + (-cos(psi)*sin(phi) + sin(psi)*sin(theta)*cos(phi))*wr + weast;
                -sin(theta)*ur + cos(theta)*sin(phi)*vr + cos(theta)*cos(phi)*wr + wdown;
                -q*wr + r*vr - g*sin(theta) - Winddot(1);
                p*wr - r*ur + g*sin(phi)*cos(theta) - Winddot(2);
                -p*vr + q*ur + g*cos(phi)*cos(theta) - Winddot(3);
                p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r;
                cos(phi)*q - sin(phi)*r;
                sin(phi)*q/cos(theta) + cos(phi)*r/cos(theta);
                p*q*Aero.Jxz*(Aero.Jx - Aero.Jy + Aero.Jz)/(Aero.Jx*Aero.Jz - Aero.Jxz^2) - q*r*(Aero.Jz*(Aero.Jz - Aero.Jy) + Aero.Jxz^2)/(Aero.Jx*Aero.Jz - Aero.Jxz^2);
                (Aero.Jz - Aero.Jx)*p*r/Aero.Jy - Aero.Jxz*(p^2 - r^2)/Aero.Jy;
                p*q*((Aero.Jx - Aero.Jy)*Aero.Jx + Aero.Jxz^2)/(Aero.Jx*Aero.Jz - Aero.Jxz^2) - q*r*Aero.Jxz*(Aero.Jx - Aero.Jy + Aero.Jz)/(Aero.Jx*Aero.Jz - Aero.Jxz^2)
                ];

            G = [
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [1/Aero.uavMass, 0, 0, 0, 0, 0];
                [0, 1/Aero.uavMass, 0, 0, 0, 0];
                [0, 0, 1/Aero.uavMass, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, 0, 0, 0];
                [0, 0, 0, Aero.Jz/(Aero.Jx*Aero.Jz - Aero.Jxz^2), 0, Aero.Jxz/(Aero.Jx*Aero.Jz - Aero.Jxz^2)];
                [0, 0, 0, 0, 1/Aero.Jy, 0];
                [0, 0, 0, Aero.Jxz/(Aero.Jx*Aero.Jz - Aero.Jxz^2), 0, Aero.Jx/(Aero.Jx*Aero.Jz - Aero.Jxz^2)]
                ];
            
            Coeffs_C = obj.coeffs_C(Aero, qbar, alpha, beta, vt, p, q, r, 0);
            Coeffs_M = obj.coeffs_M(Aero, qbar, alpha, vt, 1);
            
            controls(3) = controls(3)^2;
            
            U = Coeffs_C + Coeffs_M*controls;
            
            xdot = F + G*U;
            
            aux = cell2mat(obj.States);
            aux = aux + xdot*dt;
            obj.States = num2cell(aux);
        end
    end
end

