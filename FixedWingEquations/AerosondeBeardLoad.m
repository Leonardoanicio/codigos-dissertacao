function Aerosonde = AerosondeBeardLoad()
    % ----------------- PARAMETROS UAV MODEL ----------------------------------
    % -------------------------------------------------------------------------
    % ------------------------ GERAIS------------------------------------------

    Aerosonde = struct('uavMass', 0, 'Jx', 0, 'Jy', 0, 'Jz', 0, 'Jxz', 0, 'J', zeros(3,3), 'S', 0, 'b', 0, 'c', 0, 'Sprop', 0, 'rho', 0, 'Kmotor', 0, ...
        'Ktp', 0, 'Komega', 0, 'e', 0, 'CL0', 0, 'CD0', 0, 'Cm0', 0, 'CLalpha', 0, 'CDalpha', 0, 'Cmalpha', 0, 'CLq', 0, 'CDq', 0, 'Cmq', 0, ...
        'CLde', 0, 'CDde', 0, 'Cmde', 0, 'Cprop', 0, 'M', 0, 'alpha0', 0, 'epsilon', 0, 'CDp', 0, 'Cndr', 0, 'CY0', 0, 'Cl0', 0, 'Cn0', 0, ...
        'CYbeta', 0, 'Clbeta', 0, 'Cnbeta', 0, 'CYp', 0, 'Clp', 0, 'Cnp', 0, 'CYr', 0, 'Clr', 0, 'Cnr', 0, 'CYda', 0, 'Clda', 0, 'Cnda', 0,...
        'CYdr', 0, 'Cldr', 0, 'AR', 0);
    
    Aerosonde.uavMass = 13.5;
    Aerosonde.Jx = 0.8244;
    Aerosonde.Jy = 1.135;
    Aerosonde.Jz = 1.759;
    Aerosonde.Jxz = 0.1204;
    Aerosonde.J = [Aerosonde.Jx, 0, -Aerosonde.Jxz; 0, Aerosonde.Jy, 0; -Aerosonde.Jxz, 0, Aerosonde.Jz];
    Aerosonde.S = 0.55;
    Aerosonde.b = 2.8956;
    Aerosonde.c = 0.18994;
    Aerosonde.Sprop = 0.2027;
    Aerosonde.rho = 1.2682;
    Aerosonde.Kmotor = 80;
    Aerosonde.Ktp = 0;
    Aerosonde.Komega = 0;
    Aerosonde.e = 0.9;
    Aerosonde.CL0 = 0.28;
    Aerosonde.CD0 = 0.03;
    Aerosonde.Cm0 = -0.02338;
    Aerosonde.CLalpha = 3.45;
    Aerosonde.CDalpha = 0.3;
    Aerosonde.Cmalpha = -0.38;
    Aerosonde.CLq = 0;
    Aerosonde.CDq = 0;
    Aerosonde.Cmq = -3.6;
    Aerosonde.CLde = -0.36;
    Aerosonde.CDde = 0;
    Aerosonde.Cmde = -0.5;
    Aerosonde.Cprop = 1;
    Aerosonde.M = 50;
    Aerosonde.alpha0 = 0.4712;
    Aerosonde.epsilon = 0.1592;
    Aerosonde.CDp = 0.0437;
    Aerosonde.Cndr = -0.032;
    Aerosonde.CY0 = 0;
    Aerosonde.Cl0 = 0;
    Aerosonde.Cn0 = 0;
    Aerosonde.CYbeta = -0.98;
    Aerosonde.Clbeta = -0.12;
    Aerosonde.Cnbeta = 0.25;
    Aerosonde.CYp = 0;
    Aerosonde.Clp = -0.26;
    Aerosonde.Cnp = 0.022;
    Aerosonde.CYr = 0;
    Aerosonde.Clr = 0.14;
    Aerosonde.Cnr = -0.35;
    Aerosonde.CYda = 0;
    Aerosonde.Clda = 0.08;
    Aerosonde.Cnda = 0.06;
    Aerosonde.CYdr = -0.17;
    Aerosonde.Cldr = 0.105;
    Aerosonde.AR = Aerosonde.b^2/Aerosonde.S;
end