%% Continuous Filedbug
% Need Adriano's matlab_lib added to path
% https://github.com/adrianomcr/matlab_lib

close all, clear, clc
%% Inputs

p = [-300 300 -80]'; % Initial position
max_time = 200; %simulation time

ws = [-1 1 -1 1 -1 1]*800; % animation plot size
r_tilde = 15; % robot radius (just for plot)
animation_speed = 10; % just for plot

% f_a1 = @(x,y,z) x^2 + z^2 - 1;
% f_a2 = @(x,y,z) y - 2*x*z;

f_a1 = @(x,y,z) (2*x+2*z^2-1)^2+z^2-1;
f_a2 = @(x,y,z) y - (4*x.*z+4*z^3-3*z)/2;

O0 = [370 212 0*200 25]';
O1 = [200 -170 0*200 25]';
O2 = [-150 -130 0*200 25]';
O3 = [-570 -20 0*200 25]';
O4 = [-590 -10 0*200 25]';
O5 = [-600 0 0*200 25]';
O6 = [-610 10 0*200 25]';
O7 = [-620 20 0*200 25]';
O8 = [-630 30 0*200 25]';
O9 = [-640 40 0*200 25]';
O = [O0 O1 O2 O3 O4 O5 O6 O7 O8 O9];
O = [];
%O =[1000 0 0 1]';

MASTER_MODE = 1; % we will call auxiliar scripts
%% Selecting the curve 
fprintf('Program started\n')

dt = 10e-3;
t = 0:dt:max_time;

%% Simulation

fprintf('Simulation started\n')
pause(1)

D_data = []; % for distance plot
f_data = []; % for |V| plot
state_data = []; % for plot

vr = 23;%23; 
d_in = 120;
Tbeta = 10; % recalculated below
circ_dir = 1; % recalculated below
state = 'follow curve'; % recalculated below


% a_curve = 600; b_curve = 200; c_curve = 100;
a_curve = 500; b_curve = 500; c_curve = 100;
for k = 1:length(t)
    clc; fprintf('Simulating...\n%d%%\n',round(100*t(k)/t(end)))
    
    % find the nearest obstacle and the vector r
    near_obst = 1;
    r = [inf; inf];
    for j = 1:size(O,2) 
        new_r = O(1:2, j) - p(1:2,k);
        new_r = new_r - O(4,j)*new_r/norm(new_r);
        if norm(r) > norm( new_r ) 
           r = new_r;
           near_obst = j;
        end
    end
   
    %d_in = (r_tilde + O(4, near_obst) + 30)/sin(pi/8);
    if size(O, 2) > 0
        Tbeta = 0.5*(d_in - O(4,near_obst))/vr;
    else
        Tbeta = inf;
    end
    r = [r;0];
    
    switch state
        case 'follow curve'
            %[fc, D] = curve_vector(p(:,k), fC, k);
            [fc, D] = curve_vector_alpha2(p(:,k), a_curve, b_curve, c_curve, f_a1, f_a2);
            f = vr*fc;
            state_data(k) = 1;
            if norm(r) < d_in && r'*fc > 0
                state = 'transition to obstacle';
                Beta = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
            end
        case 'transition to obstacle'
%             [fc, D] = curve_vector(p(:,k), fC, k);
            [fc, D] = curve_vector_alpha2(p(:,k), a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p(:,k), O(:,near_obst), r, circ_dir);
            f = (1-Beta)*fc + Beta*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            state_data(k) = 2;
            if Beta > 1
                state = 'contour obstacle';
            end
        case 'contour obstacle'
%             [fc, D] = curve_vector(p(:,k), fC, k);
            [fc, D] = curve_vector_alpha2(p(:,k), a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p(:,k), O(:,near_obst), r, circ_dir);
            f = vr*fo;
            state_data(k) = 3;
            f(3) = fc(3); % keep following z component
            if r'*fc < 0
                state = 'transition to curve';
                Beta = 0;
            end
        case 'transition to curve'
%             [fc, D] = curve_vector(p(:,k), fC, k);
            [fc, D] = curve_vector_alpha2(p(:,k), a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p(:,k), O(:,near_obst), r, circ_dir);
            
            f = Beta*fc + (1-Beta)*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            state_data(k) = 4;
            if Beta > 1
                state = 'follow curve';
            end
        otherwise
            state = 'follow curve';  
    end
    
    p(:,k+1) = p(:,k) + f*dt;
    f_data(k) = norm(f);
    D_data(k) = norm(D);
end

%% Static results

%Trajectory
figure()
hold on
plot_alpha
c = distinguishable_colors(1);
%plot3(C(1,:),C(2,:),C(3,:),'k-','LineWidth',1)
grid on
axis equal
aa = find(state_data == 1);
bb = find(state_data == 2 | state_data == 4);
cc = find(state_data == 3);
plot3(p(1,aa),p(2,aa),p(3,aa),'.','Color',[0 0 1])
plot3(p(1,bb),p(2,bb),p(3,bb),'.','Color',[0.7 0 0.7])
plot3(p(1,cc),p(2,cc),p(3,cc),'.','Color',[1 0 0])

plot3(p(1,end),p(2,end),p(3,end),'.','Color',[0 0 1],'LineWidth',1,'MarkerSize',2)

for k = 1:size(O,2)
    theta = linspace(0, 2*pi, 50);
    Oi = O(:,k);
    x = Oi(4)*cos(theta) + Oi(1); y = Oi(4)*sin(theta) + Oi(2);
    plot(x,y,'k--', 'LineWidth', 1.5);
    x = (Oi(4)+d_in)*cos(theta) + Oi(1); y = (Oi(4)+d_in)*sin(theta) + Oi(2);
    plot(x,y,'y--', 'LineWidth', 1.5);
    x = (Oi(4)+40)*cos(theta) + Oi(1); y = (Oi(4)+40)*sin(theta) + Oi(2);
    plot(x,y,'g--', 'LineWidth', 1.5);
end
hold off
grid on
axis equal
xlabel('$x$','interpreter','latex','FontSize',15)
ylabel('$y$','interpreter','latex','FontSize',15)
zlabel('$z$','interpreter','latex','FontSize',15)

%Distace
figure()
hold on
plot(t,D_data(:),'-','Color',c(1,:),'LineWidth',2)
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$D$','interpreter','latex','FontSize',15)

%Velocity
figure()
hold on
plot(t,f_data(:),'.','Color',c(1,:))
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$\|V\|$','interpreter','latex','FontSize',15)

%% Animation
%animation_alpha %auxiliar script that can be run separately
clear MASTER_MODE a aa ans b bb Beta C cc circ_dir j k k0 near_obst new_r state tail Tbeta theta x y
clear O0 O1 O2 O3 O4 O5 O6 O7 O8 O9 O10 O11 O12 O13 O14 O15