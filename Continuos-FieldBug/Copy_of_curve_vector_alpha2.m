function [f, P0] = curve_vector_alpha2(p, a, b, c, f_a1, f_a2, P, kp, vr, t)
    x = p(1)/a; y = p(2)/b; z = p(3)/c;
    
    delta = 1e-4;
    
    a1_0 = f_a1(x,y,z,t);
    a1_Mx = f_a1(x+delta,y,z,t);
    a1_My = f_a1(x,y+delta,z,t);
    a1_Mz = f_a1(x,y,z+delta,t);
    g_a1 = ([a1_Mx; a1_My; a1_Mz]-a1_0)/delta;
    
	a2_0 = f_a2(x,y,z,t);
    a2_Mx = f_a2(x+delta,y,z,t);
    a2_My = f_a2(x,y+delta,z,t);
    a2_Mz = f_a2(x,y,z+delta,t);
    g_a2 = ([a2_Mx; a2_My; a2_Mz]-a2_0)/delta;
    
    P0 = P(x, y, z, t);
    g_P = ([P(x + delta, y, z, t);  P(x, y + delta, z, t);  P(x, y, z + delta, t)] - P0)/delta;
    
    G = -2/pi * atan(kp*sqrt(P0));
    H = sqrt(1 - G^2);
    
    %Feedforward
    a1_t = f_a1(x,y,z,t+delta);
    a1_dt = (a1_t - a1_0)/delta;
    
    a2_t = f_a2(x,y,z,t+delta);
    a2_dt = (a2_t - a2_0)/delta;
    
    M = [g_a1';
         g_a2';
         cross(g_a1, g_a2)'];
    a = [a1_dt;
         a2_dt;
         0;
         ];
    
    F_P = -M\a;
     
    F_c = g_P/(norm(g_P) + 1e-6);
    F_t = cross(g_a1, g_a2)/(norm(cross(g_a1,g_a2)) + 1e-9);
    
    f = G*F_c + H*F_t; f = vr*f + F_P;
end