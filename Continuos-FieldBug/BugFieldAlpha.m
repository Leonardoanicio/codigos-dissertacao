function [f, D, state, Gamma, circ_dir, near_obst, fc, fo, Tbeta] = BugFieldAlpha(p, curveType, state, Gamma, circ_dir, time, UAV)
    global fc0 fo0


    switch UAV
        case 'elevon'
            d_in = 150;
            vr = 15;
        otherwise
            d_in = 200;
            vr = 35;
    end

    dt = 0.01;
%     time = 0;
    
    %Pega a curva
    [f_a1, f_a2, P, O, O_var, kp, vcmax] = getCurve(curveType, UAV);
   
    % find the nearest obstacle and the vector r
    near_obst = 1;
    r = [inf; inf];
    for j = 1:size(O,2)
        Oj = [O(1, j) + O_var{1,j}(time); O(2, j) + O_var{2,j}(time)];
        new_r = Oj - p(1:2);
        new_r = new_r - O(4,j)*new_r/norm(new_r);
        if norm(r) > norm( new_r ) 
           r = new_r;
           near_obst = j;
        end
    end
    
    D = norm(r);
    
    if size(O, 2) > 0
        Tbeta = 0.75*(d_in - O(4, near_obst))/(vr + vcmax + O(5, near_obst));
    else
        Tbeta = inf;
    end
    r = [r;0];
    
    %Esse tem que calcular sempre, então fica aqui
    [fc, ~] = curve_vector_alpha2(p, f_a1, f_a2, P, kp, vr, time);
    fo = zeros(3, 1);
    GammaBar = 3*Gamma^2 - 2*Gamma^3;
    
    switch state
        case 1%'follow curve'
            if r'*fc > 0 && norm(r) < d_in
                state = 2;%'transition to obstacle';
                Gamma = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
                
                fc0 = fc;
                fo0 = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr, fc);    
            end
            f = fc;
            
        case 2%'transition to obstacle'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr, fc);
           
            f = (1 - GammaBar)*fc0 + GammaBar*fo;
            vr = (1 - GammaBar)*norm(fc) + GammaBar*norm(fo);
            
            f = (1 - GammaBar)*fc + GammaBar*fo;
            vr = (1 - GammaBar)*norm(fc) + GammaBar*norm(fo);
            
            f = vr*f/(1e-6 + norm(f));
%             f(3) = fc(3);
            
            Gamma = Gamma + dt/Tbeta;
            if Gamma > 1
                state = 3;%'contour obstacle';
            end
            
        case 3%'contour obstacle'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr, fc);
            f = fo;
            
            if r'*fc < 0
                state = 4;%'transition to curve';
                Gamma = 0;
                fo0 = fo;
                fc0 = fc;
            end

        case 4%'transition to curve'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr, fc);
            
            f = GammaBar*fc(1:2) + (1 - GammaBar)*fo0(1:2);
            vr = GammaBar*norm(fc(1:2)) + (1 - GammaBar)*norm(fo0(1:2));
            
            f = GammaBar*fc + (1 - GammaBar)*fo;
            vr = GammaBar*norm(fc) + (1 - GammaBar)*norm(fo);
            
            f = vr*f/(1e-6 + norm(f));
%             f(3) = fc(3);
            
            Gamma = Gamma + dt/Tbeta;
            if Gamma > 1
                state = 1;%'follow curve';
            end      
    end
    
%     f = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, 1, time, vr, fc);
%                 state = 'contour obstacle';
            
end