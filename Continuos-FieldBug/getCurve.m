function [alpha1, alpha2, P, O, O_var, kp, vcmax] = getCurve(curve, UAV)
    
    switch curve
        case 1 %Elipse
            a = 600;
            b = 400;
            u = 50;
            
            k1 = 1.5e-8;
            k2 = 1e-3;
            kp = 2000;
            
            kp = 3000;
            
            k1 = k1*kp;
            k2 = k2*kp;
            kp = 1;
            
            
            alpha1 = @(x,y,z,t) z - 100 + u.*(a^(-2).*(x + 2*t).^2 - 1);
            alpha2 = @(x,y,z,t) (x + 2*t).^2*a^(-2) + y.^2*b^(-2) - 1;
            P = @(x,y,z,t) k1*(alpha1(x,y,z,t).^2) + k2*(alpha2(x,y,z,t).^2);
           
            O = [0 -400 200 50 4
                -510 -200 200 50 4
%                 -530 190 200 50 3.5
                600 0 200 50 2
                ]';
           
            
            O_var = {
                    @(t)-2*t, @(t)50*cos(2*pi*t/200);
                    @(t)-2*t, @(t)50*sin(2*pi*t/200);
%                     @(t)-2*t, @(t)0*t;
                    @(t)-2*t, @(t)0*t;
                    }';
                
            vcmax = 0;
                
                
        case 2
            a = 600;
            b = 600;
            c = 1e4;
            k2 = 2.5;
            k1 = 0.5e-17;
            kp = 0.00001;
            
            k2 = 2.5;
            k1 = 0.5e-17;
            kp = 0.00001;
            k1 = k1*kp;
            k2 = k2*kp;
            kp = 1;
            
            alpha1 = @(x,y,z,t) (x^2 + y^2)^2 - a*(cos(2*pi*t/400)*(y^3  - 3*x^2*y) + sin(2*pi*t/400)*(x^3 - 3*x*y^2)) - b^4;
            alpha2 = @(x,y,z,t) z - 100 - (x^2 + y^2)/c;
            P = @(x,y,z,t) k1*alpha1(x,y,z,t).^2 + k2*alpha2(x,y,z,t).^2;
            
            O_var = {
                   @(t)750*cos(2*pi*(t - 50)/1200), @(t)750*sin(2*pi*(t - 50)/1200);
                   @(t)750*cos(2*pi*(t - 550)/1200), @(t)750*sin(2*pi*(t - 550)/1200);
                   @(t)475*cos(2*pi*(t - 300)/1200), @(t)475*sin(2*pi*(t - 300)/1200);
                   @(t)810*cos(2*pi*(t + 300)/1200), @(t)810*sin(2*pi*(t + 300)/1200);
                   }';
               
               
            vcmax = 4;
               
            O = [
                0 0 0 50 3.75;
                 0 0 0 50 3.75;
                0 0 0 50 1.7;
                0 0 0 50 1.7;
               ]';
           
        case 3
            1;
            
    end
end