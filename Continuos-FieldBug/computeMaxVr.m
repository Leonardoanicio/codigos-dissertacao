
%Pega a curva
[f_a1, f_a2, P, a_curve, b_curve, c_curve, O, O_var, kp] = getCurve(curveType, UAV.Type);

z = 200;
V = [];
for x=-1000:10:1000
    for y = -1000:10:1000
        for t = 0:10:200
            delta = 1e-4;

            a1_0 = f_a1(x,y,z,t);
            a1_Mx = f_a1(x+delta,y,z,t);
            a1_My = f_a1(x,y+delta,z,t);
            a1_Mz = f_a1(x,y,z+delta,t);
            g_a1 = ([a1_Mx; a1_My; a1_Mz]-a1_0)/delta;

            a2_0 = f_a2(x,y,z,t);
            a2_Mx = f_a2(x+delta,y,z,t);
            a2_My = f_a2(x,y+delta,z,t);
            a2_Mz = f_a2(x,y,z+delta,t);
            g_a2 = ([a2_Mx; a2_My; a2_Mz]-a2_0)/delta;

            %Feedforward
            a1_t = f_a1(x,y,z,t+delta);
            a1_dt = (a1_t - a1_0)/delta;

            a2_t = f_a2(x,y,z,t+delta);
            a2_dt = (a2_t - a2_0)/delta;

            M = [g_a1';
             g_a2';
             cross(g_a1, g_a2)'];
            a = [a1_dt;
                 a2_dt;
                 0;
                 ];
             V = [V norm(M\a)];
        end
    end
end