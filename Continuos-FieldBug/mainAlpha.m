%% Continuous Filedbug
% Need Adriano's matlab_lib added to path
% https://github.com/adrianomcr/matlab_lib

close all, clear, clc
%% Input

curveType = 2; 

p = [500 800 120]'; % Initial position
max_time = 200; %simulation time

ws = [-1 1 -1 1 -1 1]*700; % animation plot size
r_tilde = 15; % robot radius (just for plot)
animation_speed = 10; % just for plot

MASTER_MODE = 1; % we will call auxiliar scripts
%% Selecting the curve 
fprintf('Program started\n')

dt = 10e-3;
t = 0:dt:max_time;
ds = pi/100;
s = 0:ds:2*pi;

%% Simulation

fprintf('Simulation started\n')
pause(1)

D_data = []; % for distance plot
f_data = []; % for |V| plot
field = [];
fo = [];
fc = [];
state_data = []; % for plot

vr = 30; 
d_in = 150;
Tbeta = 10; % recalculated below
circ_dir = 1; % recalculated below
state = 1; % recalculated below
Beta = 0;
D2 = inf;
for k = 1:length(t)
    if mod(k, 10) == 0
        clc; fprintf('Simulating...\n%d%%\n',round(100*t(k)/t(end)))
    end
    
%     state = 1;
    [f, D, state, Beta, circ_dir, near_obst, Fc, Fo] = BugFieldAlpha(p(:, k), curveType, state, Beta, circ_dir, k*dt, '');  
    
    if D < D2
        D2 = D;
    end
    state_data(k) = state;
    
    p(:,k+1) = p(:,k) + f*dt;
    field = [field, f];
    fo = [fo, Fo];
    fc = [fc, Fc];
    f_data(k) = norm(f);
    D_data(k) = norm(D);
end

%% Static results
%Quiver
u = field(1, :);
v = field(2, :);
w = field(3, :);
% quiver3(p(1, 1:end-1), p(2, 1:end-1), p(3, 1:end-1), u, v, w);
%Trajectory
% figure()
% hold on
c = distinguishable_colors(1);

aa = find(state_data == 1);
bb = find(state_data == 2 | state_data == 4);
cc = find(state_data == 3);
plot3(p(1,aa),p(2,aa),p(3,aa),'.','Color',[0 0 1]); hold
plot3(p(1,bb),p(2,bb),p(3,bb),'.','Color',[0.7 0 0.7])
plot3(p(1,cc),p(2,cc),p(3,cc),'.','Color',[1 0 0])

plot_alpha(curveType, 0);

% plot3(p(1,end),p(2,end),p(3,end),'.','Color',[0 0 1],'LineWidth',1,'MarkerSize',25)
