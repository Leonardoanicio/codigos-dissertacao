function f = obstacle_vector_alpha(p, Oi, Oix_var, Oiy_var, r, dir, time, vr, fc)
     
    kp = 2.5e-4;
%     kp = 3e-4;
    dt = 1e-3;
    
    R = 25 + Oi(4);
    
    dalphadx = 2*(p(1) - Oi(1) - Oix_var(time));
    dalphady = 2*(p(2) - Oi(2) - Oiy_var(time));
    
    alpha_0 = (p(1) - Oi(1) - Oix_var(time))^2 + (p(2) - Oi(2) - Oiy_var(time))^2 - R^2;
    alpha_Mt = (p(1) - Oi(1) - Oix_var(time + dt))^2 + (p(2) - Oi(2) - Oiy_var(time + dt))^2 - R^2;
    dalphadt = (alpha_Mt - alpha_0)/dt;
    
    G = -2/pi * atan(kp*alpha_0); 
    H = sqrt(1 - G^2);
    
    Da = [dalphadx dalphady]';
    DHa = [-dalphady dalphadx]';
    
    M = [Da'; DHa'];
    
    a = [dalphadt 0]';
    
    f_vt = -inv(M)*a;
    
    f_s = [vr*(G*Da + dir*H*DHa)/(norm(Da) + 1e-6); fc(3)];
    f_s = vr*f_s/sqrt(vr^2 + fc(3)^2);
    
    f =  f_s + [f_vt; 0];
    
    vxy = sqrt(vr^2 - fc(3)^2);
    f_xy = [vxy*(G*Da + dir*H*DHa)/(norm(Da) + 1e-6); fc(3)];
    f = f_xy + [f_vt; 0];
end
