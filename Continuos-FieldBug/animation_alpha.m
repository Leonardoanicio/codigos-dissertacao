%figure('units','normalized','outerposition',[0 0 1 1]) %full window size
figure() %normal figure
%h0 = plot3(C(1,:),C(2,:),C(3,:),'k-','LineWidth',2);
hold on
plot_alpha

hl = [];
hr = [];
%hre = [];


hl = plot3(p(1,1),p(2,1),p(3,1),'-','Color',c(1,:),'LineWidth',2);
hr = plot_dodecahedron(p(1:3,1),r_tilde,c(1,:),0.7,'-');
%hre = plot_dodecahedron(p(1:3,1),1.1*r_tilde,c(1,:),0.1,'none');


for k = 1:size(O,2)
    Oi = O(:,k);
    H = [eye(3) Oi(1:3); [0 0 0 1]];
    plot_cylinder(H, 300, Oi(4), [0.5 0 0], 0.5, 11, '-')   
end

hold off
grid on
axis equal
axis(ws)
xlabel('$x$','interpreter','latex','FontSize',15)
ylabel('$y$','interpreter','latex','FontSize',15)
zlabel('$z$','interpreter','latex','FontSize',15)
tail = round(1000/1);
title(['t = ', '0.00'])
%view(2)



pause(1)

for k = 1:animation_speed:length(t)
    title(['t = ', num2str(t(k), '%.2f')])
    k0 = max([1, k-tail]);
    
%     C = fC(s,k);
%     set(h0, 'XData', C(1,:), 'YData', C(2,:));
    
    set(hl,'XData',p(1,k0:k),'YData',p(2,k0:k),'ZData',p(3,k0:k));
    set_dodecahedron(p(1:3,k),hr,r_tilde);
    %set_dodecahedron(p(1:3,k),hre,1.1*r_tilde);
    
    switch state_data(k)
        case 1 % follow curve
            set(hr, 'FaceColor', [0 0 1]);
        case {2,4}
            set(hr, 'FaceColor', [0.7 0 0.7]);
        case 3
            set(hr, 'FaceColor', [1 0 0]);
    end

    drawnow
end
title(['t = ', num2str(max_time, '%.2f')])