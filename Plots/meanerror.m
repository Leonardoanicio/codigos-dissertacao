% load('/home/leonardo/MATLAB/projects/codigos-dissertacao/Resultados/Aerosonde/WithNoise/ABV/C2/Result.mat')
load('/home/leonardo/MATLAB/projects/codigos-dissertacao/Resultados/Zagi/ZAGI_ABV.mat')

X = Result{1};
FInfo = Result{6};

dt = 0.01;

dpn = diff(X(1, :))/dt;
dpe = diff(X(2, :))/dt;
dpd = diff(X(3, :))/dt;
V = [dpn; dpe; -dpd];
e1 = mean(abs(vecnorm(V) - vecnorm(FInfo.F(:, 1:end-1))), 'all');

psi = unwrap(atan2(dpe, dpn));
F_psi = unwrap(atan2(FInfo.F(2, :), FInfo.F(1, :)));
e2 = mean(abs(wrapToPi(psi - F_psi(1:end-1))), 'all');

fprintf("deg: %f\n", rad2deg(e2));
fprintf("m/s: %f\n", e1);