vert = [
            0 0 0;
            0.5 1 0;
            1 0 0;
            0 0 0.5;
            0.5 1 0.5;
            1 0 0.5;
            ];
 faces = [
%             1 2 3 3;
            4 5 6 6;
            1 2 5 4;
            2 3 6 5;
         ];
            
        
patch('Vertices',vert,'Faces',faces, 'FaceColor', 'red', 'FaceAlpha', 0.5, 'EdgeAlpha', 0.1);
hold;
plotCylinder(0.5, 0.5, 0.5, 1, 'blue', 0.2);
xticks([]);
yticks([]);
zticks([]);
xlabel('x_1');
ylabel('x_2');
zlabel('x_3');
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
view(3)
axis vis3d   
axis equal