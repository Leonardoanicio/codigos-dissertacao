hold
F_psi = unwrap(F_psi);
F_psi = [dpd'; dpd(end)];
F_psi = FInfo.F(3, :);
F_psi = Re(6, :);
plot(t(FInfo.state == 1), F_psi(FInfo.state == 1), '.g'); 
plot(t(FInfo.state == 3), F_psi(FInfo.state == 3), '.r');
plot(t(FInfo.state == 2 | FInfo.state == 4), F_psi(FInfo.state == 2 | FInfo.state == 4), '.b');