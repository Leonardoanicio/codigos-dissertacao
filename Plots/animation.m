load('/home/leonardo/projects/dissertacao/Resultados/Aerosonde/WithoutNoise/ABV/C1/Result.mat')
X = Result{1};
FInfo = Result{6};

dt = 0.01;
t = linspace(0, length(X)*dt, length(X));

close all;

dec_rate = 1;
time = t;

P_North = X(1, 1:dec_rate:end);
P_East = X(2, 1:dec_rate:end);
h = -X(3, 1:dec_rate:end);

roll = X(7, 1:dec_rate:end);
pitch = X(8, 1:dec_rate:end);
yaw = X(9, 1:dec_rate:end) - pi/2; 


% Print the trajectory of the UAV
% figure(1)
figure('units','normalized','outerposition',[0 0 1 1])
plot3(P_East/1e3,P_North/1e3,h/1e3,'-','Color',[1 0 0],'LineWidth',1);
hold on;
% legend('Aircraft','Target');
plot3(P_East(1)/1e3,P_North(1)/1e3,h(1)/1e3,'o','Color',[1 0 0],'LineWidth',2,'MarkerSize',12);
hold off;
xlabel('East Position (km)');
ylabel('North Position (km)');
zlabel('Height (km)');
grid on;
% title('Trajectory');
axis equal;


p = [P_East; P_North; h]/1e3;
axis equal
w_s = [min(P_East) max(P_East) min(P_North) max(P_North) min(h) max(h)]/1e3 + [-1 1 -1 1 -1 1]*0.1;
axis(w_s);


% ANIMATION
% ----------  ----------  ----------  ----------  ----------  ----------  ----------
% figure(1)
hold on
h1 = fill3(0,0,0,'b','LineWidth',2); h1.LineStyle = 'none';
h2 = fill3(0,0,0,'b','LineWidth',2); h2.LineStyle = 'none';
h3 = fill3(0,0,0,'b','LineWidth',2); h3.LineStyle = 'none';
h4 = fill3(0,0,0,'b','LineWidth',2); h4.LineStyle = 'none';
h5 = fill3(0,0,0,'b','LineWidth',2); h5.LineStyle = 'none';
h6 = fill3(0,0,0,'b','LineWidth',2); h6.LineStyle = 'none';

h7 = fill3(0,0,0,'b','LineWidth',2); h7.LineStyle = 'none';
h8 = fill3(0,0,0,'b','LineWidth',2); h8.LineStyle = 'none';
h9 = fill3(0,0,0,'b','LineWidth',2); h9.LineStyle = 'none';
grid on
hold off
uav_e = 0.07;
uav_size = 20; %uav_size = uav_size^1;
UAV0 = [[0 0.5 0 0.2   0 0.5 0 0.2];[1 0 -1 0   1 0 -1 0]; [1 1 1 1   -1 -1 -1 -1]*uav_e]/uav_size;
TAIL0 = [[0.2 -0.7 -0.7 -0.55 -0.4 0.2     0.2 -0.7 -0.7 -0.55 -0.4 0.2];[1 1 1 1 1 1     -1 -1 -1 -1 -1 -1]*uav_e;[-1 -1 3 3 1 1     -1 -1 3 3 1 1]*uav_e]/uav_size;
k = 1;
R = [cos(yaw(k)) sin(yaw(k)) 0; -sin(yaw(k)) cos(yaw(k)) 0; 0 0 1];
Rroll = [1 0 0; 0 cos(roll(k)) sin(roll(k)); 0 -sin(roll(k)) cos(roll(k))];
Rpitch = [cos(pitch(k)) 0 -sin(pitch(k)); 0 1 0; sin(pitch(k)) 0 cos(pitch(k))];

UAV = Rroll*Rpitch*R*UAV0;
TAIL = Rroll*Rpitch*R*TAIL0;

set(h1,'XData',UAV(1,1:4)+p(1,k),'YData',UAV(2,1:4)+p(2,k),'ZData',UAV(3,1:4)+p(3,k));
set(h2,'XData',UAV(1,5:8)+p(1,k),'YData',UAV(2,5:8)+p(2,k),'ZData',UAV(3,5:8)+p(3,k));
set(h3,'XData',UAV(1,[1 5 6 2])+p(1,k),'YData',UAV(2,[1 5 6 2])+p(2,k),'ZData',UAV(3,[1 5 6 2])+p(3,k));
set(h4,'XData',UAV(1,[2 6 7 3])+p(1,k),'YData',UAV(2,[2 6 7 3])+p(2,k),'ZData',UAV(3,[2 6 7 3])+p(3,k));
set(h5,'XData',UAV(1,[3 7 8 4])+p(1,k),'YData',UAV(2,[3 7 8 4])+p(2,k),'ZData',UAV(3,[3 7 8 4])+p(3,k));
set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
set(h7,'XData',TAIL(1,1:6)+p(1,k),'YData',TAIL(2,1:6)+p(2,k),'ZData',TAIL(3,1:6)+p(3,k));
set(h8,'XData',TAIL(1,7:12)+p(1,k),'YData',TAIL(2,7:12)+p(2,k),'ZData',TAIL(3,7:12)+p(3,k));
set(h9,'XData',TAIL(1,[1 2 8 7])+p(1,k),'YData',TAIL(2,[1 2 8 7])+p(2,k),'ZData',TAIL(3,[1 2 8 7])+p(3,k));
pause(1)
set(1,'Position',[0.3542 0.0324 0.6354 0.6806])
for k = 1:5:round(length(p(1,:))/1)
    R = [cos(yaw(k)) sin(yaw(k)) 0; -sin(yaw(k)) cos(yaw(k)) 0; 0 0 1];
    Rroll = [1 0 0; 0 cos(roll(k)) sin(roll(k)); 0 -sin(roll(k)) cos(roll(k))];
    Rpitch = [cos(pitch(k)) 0 -sin(pitch(k)); 0 1 0; sin(pitch(k)) 0 cos(pitch(k))];
    UAV = Rroll*Rpitch*R*UAV0;
    TAIL = Rroll*Rpitch*R*TAIL0;


    set(h1,'XData',UAV(1,1:4)+p(1,k),'YData',UAV(2,1:4)+p(2,k),'ZData',UAV(3,1:4)+p(3,k));
    set(h2,'XData',UAV(1,5:8)+p(1,k),'YData',UAV(2,5:8)+p(2,k),'ZData',UAV(3,5:8)+p(3,k));
    set(h3,'XData',UAV(1,[1 5 6 2])+p(1,k),'YData',UAV(2,[1 5 6 2])+p(2,k),'ZData',UAV(3,[1 5 6 2])+p(3,k));
    set(h4,'XData',UAV(1,[2 6 7 3])+p(1,k),'YData',UAV(2,[2 6 7 3])+p(2,k),'ZData',UAV(3,[2 6 7 3])+p(3,k));
    set(h5,'XData',UAV(1,[3 7 8 4])+p(1,k),'YData',UAV(2,[3 7 8 4])+p(2,k),'ZData',UAV(3,[3 7 8 4])+p(3,k));
    set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
    set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
    set(h7,'XData',TAIL(1,1:6)+p(1,k),'YData',TAIL(2,1:6)+p(2,k),'ZData',TAIL(3,1:6)+p(3,k));
    set(h8,'XData',TAIL(1,7:12)+p(1,k),'YData',TAIL(2,7:12)+p(2,k),'ZData',TAIL(3,7:12)+p(3,k));
    set(h9,'XData',TAIL(1,[1 2 8 7])+p(1,k),'YData',TAIL(2,[1 2 8 7])+p(2,k),'ZData',TAIL(3,[1 2 8 7])+p(3,k));
%     pause(0.1)

    set(1,'Position',[0.3542 0.0324 0.6354 0.6806])
    drawnow
end
% ----------  ----------  ----------  ----------  ----------  ----------  ----------

% % 
% % %%
% % % % alpha(x,y) = a*x^4 - b*x^2*y^2 + c*y^4 - 1000 = 0;
% % a = 1;
% % b = 2;
% % c = 1;
% % t = 0:2*pi/200:2*pi; 
% % r = (1000./(a*cos(t).^4-b*cos(t).^2.*sin(t).^2+c*sin(t).^4)).^0.25;
% % x = r.*cos(t);
% % y = r.*sin(t);
% % plot(x,y)
% % axis equal
% % axis([-1 1 -1 1]*7)
% % grid on
% % 
% % 
% % 
% % x = -20:0.1:20;
% % y = -20:0.1:20;
% % [X,Y] = meshgrid(x,y);
% % 
% % 
% % ALPHA = X*0;
% % for i = 1:1:length(x)
% %     xn = x(i);
% %     for j = 1:1:length(y)
% %     yn = y(j);
% %     
% %         ALPHA(j,i) = a*xn^4 - b*xn^2*yn^2 + c*yn^4 - 1000;
% %     
% %     end
% % end
% % 
% % surf(X,Y,ALPHA,'LineStyle','none')
% % 
% % 
% % t = 0:2*pi/200:2*pi; 
% % r = (1000./(a*cos(t).^4-b*cos(t).^2.*sin(t).^2+c*sin(t).^4)).^0.25;
% % x = r.*cos(t);
% % y = r.*sin(t);
% % hold on
% % plot3(x,y,y*0,'LineWidth',3)
% % hold off
% % % axis equal
% % axis([-1 1 -1 1]*20)
% % grid on
