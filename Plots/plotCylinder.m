function OB = plotCylinder(cx, cy, h, R, color, FaceAlpha)

[X, Y, Z] = cylinder([R R]);
X = X + cx; Y = Y + cy; Z = h*Z;



OB = mesh(X, Y, Z);

OB.LineStyle = 'none';
OB.FaceColor = color;
%OB.EdgeColor = color;
OB.FaceAlpha = FaceAlpha;

end