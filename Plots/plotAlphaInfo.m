function [Figs] = plotAlphaInfo(X, U, E, EKF, FInfo, t, curveType, finalPlot, animate, finalVideo)
    %Pega a curva
    [f_a1, f_a2, P, a_curve, b_curve, c_curve, O, O_var, kp] = getCurve(curveType);

    pn = X(1,:);
    pe = X(2,:);
    pd = X(3,:);
    ur = X(4,:);
    vr = X(5,:);
    wr = X(6,:);
    phi = X(7,:);
    theta = X(8,:);
    psi = X(9,:);
    p = X(10,:);
    q = X(11,:);
    r = X(12, :);
    
    vt = vecnorm([ur; vr; wr]);

    if finalPlot
        %Plota os controles
        fU = figure();
        subplot(2, 2, 1);
        plot(t, U(1, :), 'LineWidth', 1);        legend('Aileron');
            grid on;
        subplot(2, 2, 2);
        plot(t, U(2, :), 'LineWidth', 1);        legend('Elevator');
            grid on;
        subplot(2, 2, 3);
        plot(t, U(3, :), 'LineWidth', 1);        legend('Rudder');
            grid on;
        subplot(2, 2, 4);
        plot(t, U(4, :), 'LineWidth', 1);        legend('Throttle');
            grid on;
        sgtitle('Controls');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
        
        %Plot as velocidades
        fVel = figure();
        subplot(2, 2, 1);
        plot(t, vt, 'LineWidth', 1);              legend('v_{true}');
            grid on;
        subplot(2, 2, 2);
        plot(t, ur, 'LineWidth', 1);               legend('u_r');
            grid on;
        subplot(2, 2, 3);
        plot(t, vr, 'LineWidth', 1);               legend('v_r');
            grid on;
        subplot(2, 2, 4);
        plot(t, wr, 'LineWidth', 1);               legend('w_r');
            grid on;
        sgtitle('Velocities');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
         
        fErr = figure();
        subplot(3, 2, 1);
        plot(t, E(1, :), 'LineWidth', 1);              
        legend('\textbf{$\alpha$}', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('rad', 'FontSize', 15);
        grid on;
        subplot(3, 2, 3);
        plot(t, E(2, :), 'LineWidth', 1);               
        legend('\textbf{$\beta$}', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('rad', 'FontSize', 15);
        grid on;
        subplot(3, 2, 5);
        plot(t, E(3, :), 'LineWidth', 1);               
        legend('$v_{true}$', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('m/s', 'FontSize', 15);
        grid on;
        subplot(3, 2, 2);
        plot(t, E(4, :), 'LineWidth', 1);               
        legend('\textbf{$\dot{\phi}$}', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('rad/s', 'FontSize', 15);
        grid on;
        subplot(3, 2, 4);
        plot(t, E(5, :), 'LineWidth', 1);               
        legend('\textbf{$\dot{\theta}$}', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('rad/s', 'FontSize', 15);
        grid on;
        subplot(3, 2, 6);
        plot(t, E(6, :), 'LineWidth', 1);               
        legend('\textbf{$\dot{\psi}$}', 'interpreter', 'latex', 'FontSize', 15);
        ylabel('rad/s', 'FontSize', 15);
        grid on;
        sgtitle('MPC Error');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
        
        %Plot os erros do EKF
        fEKF = figure();
        subplot(3, 3, 1);
        plot(t, pe - EKF(2, :), 'LineWidth', 1);
        title('Pe (m)');
        title('Position Error');
        grid on;
        subplot(3, 3, 4);
        plot(t, pn - EKF(1, :), 'LineWidth', 1); 
        title('Pn (m)');
        grid on;
        subplot(3, 3, 7);
        plot(t, pd - EKF(3, :),'LineWidth', 1);
        title('Pd (m)');
        grid on;
        subplot(3, 3, 2);
        plot(t, ur - EKF(4, :), 'LineWidth', 1);               
        title('Ur (m/s)');
        title('Velocity Error');
        grid on;
        subplot(3, 3, 5);
        plot(t, vr - EKF(5, :), 'LineWidth', 1);               
        title('Vr (m/s)');
        grid on;
        subplot(3, 3, 8);
        plot(t, wr - EKF(6, :), 'LineWidth', 1);               
        title('Wr (m/s)');
        grid on;
        subplot(3, 3, 3);
        plot(t, phi - EKF(7, :), 'LineWidth', 1);               
        title('\phi (rad)');
        title('Attitude Error');
        grid on;
        subplot(3, 3, 6);
        plot(t, theta - EKF(8, :), 'LineWidth', 1);               
        title('\theta (rad)');
        subplot(3, 3, 9);
        plot(t, psi - EKF(9, :), 'LineWidth', 1);               
        title('\psi (rad)');
        grid on;
        sgtitle('EKF Errors');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        drawnow;

        %Plota a curva com os dados
        fTraj = figure('units','normalized','outerposition',[0 0 1 1]);
        hold on;
        rc = plot_alpha(curveType);
        aa = find(FInfo.state == 1);
        bb = find(FInfo.state == 2 | FInfo.state == 4);
        cc = find(FInfo.state == 3);
        plot3(pe(aa), pn(aa), -pd(aa), '.', 'Color', [0 0 1]);
        pa = plot3(0,0,0, 'Color', [0 0 1]);
        plot3(pe(bb), pn(bb), -pd(bb), '.', 'Color', [0.7 0 0.7]);
        pb = plot3(0,0,0, 'Color', [0.7 0 0.7]);
        plot3(pe(cc), pn(cc), -pd(cc), '.', 'Color', [1 0 0]);
        pc = plot3(0,0,0, 'Color', [1 0 0]);
        
        zmin = min(-pd);
        angle = linspace(0, 2*pi, 50);
        zz = zmin*ones(size(angle));
        
        %Plota os obstaculos
        for k = 1:size(O, 2)
            Oi = O(:,k);
            x = Oi(1);
            y = Oi(2);
            r = Oi(4);
            
            %Plota o obstáculo
            xx = x + r*cos(angle);
            yy = y + r*sin(angle);
            plot3(xx, yy, zz, 'Color', [0 0 0]);

            %Curva de referencia
            xx = x + (r + 50)*cos(angle);
            yy = y + (r + 50)*sin(angle);
            plot3(xx, yy, zz, 'Color', [1 1 0]);

            %Din
            xx = x + (r + 150)*cos(angle);
            yy = y + (r + 150)*sin(angle);
            plot3(xx, yy, zz, 'Color', [0 1 0]);
        end
        %zlim([min(-pd) max(-pd)]);
        fake_legend_curve = plot3(1e6, 1e6, 1e6, 'k-');
        axis equal; axis([min(pe)-100 max(pe)+100 min(pn)-100 max(pn)+100 min(-pd)-100 max(-pd)+100]);
        view(2)
        legend([pa, pb, pc, fake_legend_curve], 'Follow Curve', 'Transition', 'Contour Obstacle', 'Curve');
        drawnow;

        if animate
            fAnimation = figure('units','normalized','outerposition',[0 0 1 1]);
            figure(fAnimation); hold on; view(2); axis equal; axis([min(pe)-50 max(pe)+50 min(pn)-50 max(pn)+50 min(-pd)-50 max(-pd)+50]);
            %plot3(C(1, :), C(2, :), C(3, :), 'k');
            rc = plot_alpha(curveType);
            fake_legend_curve = plot3(1e6, 1e6, 1e6, 'k-');
            
            drawnow;
            POB = [];
            POB_ref = [];
            POB_Din = [];
            angle = linspace(0, 2*pi, 50);
            zz = zmin*ones(size(angle));
            for k = 1:size(O, 2)
                Oi = O(:,k);
                x = Oi(1);
                y = Oi(2);
                r = Oi(4);

                %Plota o obstáculo
                xx = x + r*cos(angle);
                yy = y + r*sin(angle);
                POB = [POB plot3(xx, yy, zz, '--', 'Color', [0 0 0])];

                %Curva de referencia
                xx = x + (r + 50)*cos(angle);
                yy = y + (r + 50)*sin(angle);
                POB_ref = [POB_ref plot3(xx, yy, zz, '--', 'Color', [1 1 0])];

                %Din
                xx = x + (r + 150)*cos(angle);
                yy = y + (r + 150)*sin(angle);
                POB_Din = [POB_Din plot3(xx, yy, zz, '--', 'Color', [0 1 0])];
            end

            hold on;
            if finalVideo
                V = VideoWriter('curve.avi');
                V.Quality = 75;
                V.FrameRate = 25;
                open(V);
            end
            p1 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 10, 'MarkerFaceColor', 'blue'); 
            %p2 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'red'); 
            f1 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'green', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            f2 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'blue', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            f3 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'red', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            
            for t=1:30:length(pe)
                if FInfo.state(t) == 1
                    p1.MarkerFaceColor = [0 0 1];
                elseif FInfo.state(t) == 3
                    p1.MarkerFaceColor = [1 0 0];
                else
                    p1.MarkerFaceColor = [0.7 0 0.7];
                end
                
                for k = 1:size(O, 2)
                    Oi = O(:,k);
                    Oix_var = O_var{1,k};
                    Oiy_var = O_var{2,k};
                    x = Oi(1) + Oix_var(t*0.01);
                    y = Oi(2) + Oiy_var(t*0.01);
                    xx = x + r*cos(angle);
                    yy = y + r*sin(angle);
                    POB(k).XData = xx;
                    POB(k).YData = yy;
                    
                    xx = x + (r + 50)*cos(angle);
                    yy = y + (r + 50)*sin(angle);
                    POB_ref(k).XData = xx;
                    POB_ref(k).YData = yy;
                    
                    xx = x + (r + 150)*cos(angle);
                    yy = y + (r + 150)*sin(angle);
                    POB_Din(k).XData = xx;
                    POB_Din(k).YData = yy;
                end
                
                f = FInfo.F(:, t);
                fc = FInfo.Fc(:, t);
                fo = FInfo.Fo(:, t);
                
                f = 50*f/(1e-3 + norm(f));
                fc = 50*fc/(1e-3 + norm(fc));
                fo = 50*fo/(1e-3 + norm(fo));
                
                p1.XData = pe(t);
                p1.YData = pn(t);
                p1.ZData = -pd(t);
                
                %p2.XData = FInfo.nearp(1, t);
                %p2.YData = FInfo.nearp(2, t);
                %p2.ZData = FInfo.nearp(3, t);
                
                f1.XData = pe(t); f1.YData = pn(t); f1.ZData = -pd(t);
                f2.XData = pe(t); f2.YData = pn(t); f2.ZData = -pd(t);
                f3.XData = pe(t); f3.YData = pn(t); f3.ZData = -pd(t);
                
                f1.UData = f(1); f1.VData = f(2); f1.WData = 1;
                f2.UData = fc(1); f2.VData = fc(2); f2.WData = 1;
                f3.UData = fo(1); f3.VData = fo(2); f3.WData = 1;
                
%                 legend([p1, p2, f1, f2, f3, POB(1), rc, din],'UAV', 'Closest Point', ...
%                     'Total Field', 'Curve Field', 'Obstacle Field',...
%                      'Obstacles','Safe Zone', 'D_{in}');
                legend([p1, f1, f2, f3, POB(1), POB_Din(1), fake_legend_curve],'UAV', ...
                    'Total Field', 'Curve Field', 'Obstacle Field',...
                     'Obstacles', 'D_{in}', 'Curve');
                drawnow;

                for pob = POB
                    pob.Color = [0 0 0];
                end

                if FInfo.state(t) >= 2
                    POB(FInfo.nearo(t)).Color = [1 0 0];
                end
                if finalVideo
                    writeVideo(V, getframe(gcf));
                end
            end
            figure(fTraj);
            drawnow;
            if finalVideo
                for i=1:V.FrameRate*2
                    writeVideo(V, getframe(gcf));
                end
                close(V);
            end         
        end
    end
    set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
    set(findall(gcf,'-property','FontSize'),'FontSize',12);
    grid on;
    if animate
        Figs = [fTraj, fAnimation, fU, fVel];
    else
        Figs = [fTraj, fU, fVel];
    end
end