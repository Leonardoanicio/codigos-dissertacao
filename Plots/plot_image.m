
%World
ws = [-3 6 -2.5 2.8];

%Curve
a = 2;
b = 1;
cx = 0;
cy = 0;

%Obstacles
Obsts = struct('c',[],'r',0,'r_c',0,'din',0);
Obsts(1) = struct('c',[-0.5; 1],'r',0.2,'r_c',0.65,'din',1.1);
Obsts(2) = struct('c',[4.0; -1.0],'r',0.3,'r_c',0.6,'din',0.9);


%Robot
p0 = [2; 2];
F = [-1; -0.1];
F = F/norm(F);
Fo = [-1; -0.75];
Fo = Fo/norm(Fo);

f_C = @(s)[a*cos(s)+cx; b*sin(s)+cy];


sv = linspace(0,2*pi,200);

C = f_C(sv);

figure(1)
set(1,'Position',[91 114 735 510])

plot(C(1,:), C(2,:), 'k-', 'LineWidth',2)
text(2.1, 0.10, '$\mathcal{C}$','Interpreter','latex','FontSize',14)

grid on;
axis equal
axis(ws)

sv2 = linspace(0,2*pi,50);

hold on
for k = 1:1:length(Obsts)
    
    O = [Obsts(k).r*cos(sv2)+Obsts(k).c(1); Obsts(k).r*sin(sv2)+Obsts(k).c(2)];
    fill(O(1,:), O(2,:),'k','FaceColor',[0.5 0.1 0.0],'LineStyle','none')
    
    O = [Obsts(k).r_c*cos(sv2)+Obsts(k).c(1); Obsts(k).r_c*sin(sv2)+Obsts(k).c(2)];
    plot(O(1,:),O(2,:),'y-','LineWidth',2)
    
    O = [Obsts(k).din*cos(sv2)+Obsts(k).c(1); Obsts(k).din*sin(sv2)+Obsts(k).c(2)];
    plot(O(1,:),O(2,:),'g--','LineWidth',2)

end
hold off


hold on

plot(p0(1),p0(2),'k.','MarkerSize',25')

vec = Obsts(1).c-p0;
quiver(p0(1),p0(2),vec(1),vec(2),'k','LineWidth',1.5,'MaxHeadSize',0.6,'AutoScale','off')
text(0.6, 1.65, '$r_i$','Interpreter','latex','FontSize',14)

quiver(p0(1),p0(2),F(1),F(2),'b','LineWidth',2,'MaxHeadSize',0.6,'AutoScale','off')
text(0.8, 2.2, '$F_c(x,t)$','Interpreter','latex','FontSize',14)

quiver(p0(1),p0(2),Fo(1),Fo(2),'r','LineWidth',2,'MaxHeadSize',0.6,'AutoScale','off')
text(1.2, 1.2, '$F_o(x,t)$','Interpreter','latex','FontSize',14)

line = [Obsts(1).c, Obsts(1).c+Obsts(1).din*[0; -1]];
plot(line(1,:),line(2,:), 'k-', 'LineWidth',1.5)
text(-0.45, 0.15, '$d_{in}$','Interpreter','latex','FontSize',14)

line = [Obsts(1).c, Obsts(1).c+Obsts(1).r_c*[-1/sqrt(2); 1/sqrt(2)]];
plot(line(1,:),line(2,:), 'k-', 'LineWidth',1.5)
text(-1.1, 1.15, '$R_i$','Interpreter','latex','FontSize',14)

vec = [cos(pi/4); sin(pi/4)]*1.5;
quiver(Obsts(2).c(1),Obsts(2).c(2),vec(1),vec(2),'k-','LineWidth',1,'MaxHeadSize',0.6,'AutoScale','off')
text(5.1, 0.2, '$\mathcal{O}_i$','Interpreter','latex','FontSize',14)

vec = [cos(9*pi/8); sin(9*pi/8)]*1.0;
vec_u = vec/norm(vec);
quiver(Obsts(2).c(1)+Obsts(2).r_c(1)*vec_u(1),Obsts(2).c(2)+Obsts(2).r_c(1)*vec_u(2),vec(1),vec(2),'k-','LineWidth',1,'MaxHeadSize',0.6,'AutoScale','off')
text(2.2, -1.7, '$\mathcal{C}_i$','Interpreter','latex','FontSize',14)


hold off



axis off
set(1,'Color',[1 1 1])