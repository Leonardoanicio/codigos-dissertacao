function graf = plot_alpha(curve_type, t)
[X, Y, Z] = meshgrid(linspace(-1200,900,200), linspace(-1200, 900, 200), linspace(50, 200, 100));

switch curve_type
    case 1
        a = 600;
        b = 400;
        u = 50;
        W = ((X + 2*t)./a).^2 + (Y./b).^2 - 1;
        Z = 100 - u*(((X + 2*t)/a).^2 - 1);
        
    case 2
        a = 600;
        b = 600;
        c = 1e4;
        W = (X.^2 + Y.^2).^2 - a*(cos(2*pi*t / 400)*(Y.^3 - 3*X.^2.*Y) + sin(2*pi*t / 400)*(X.^3 - 3*X.*Y.^2)) - b^4;
        Z = 100 + (X.^2 + Y.^2)/c;
end
        
sup = isosurface(X, Y, Z, W, 0);
graf = patch(sup, 'LineWidth', 2);
end