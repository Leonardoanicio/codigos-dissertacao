%Sinais de controle
t_f = 200;
figure;
t = linspace(0, t_f, size(X, 2));
dt = 0.01;
switch UAV.Type
    case "elevon"
        subplot(2, 1, 1, 'Units', 'normalized', 'Position', [0.125, 0.55, 0.8, 0.35]);
        hold;
        plot(t, U(3, :), 'LineWidth', 1.5); grid; 
        plot([t(1) t(end)], [UAV.ControlBounds(3, 1) UAV.ControlBounds(3, 1)], 'r--', 'LineWidth', 1.5);
        plot([t(1) t(end)], [UAV.ControlBounds(3, 2) UAV.ControlBounds(3, 2)], 'r--', 'LineWidth', 1.5);
        ylim(flip(UAV.ControlBounds(3, :)*1.1));
        ylabel('%'); legend('Throttle'); 
        xticklabels([]);
        subplot(2, 1, 2, 'Units', 'normalized', 'Position', [0.125, 0.15, 0.8, 0.35]);
        hold;
        plot(t, 180/pi*U(1, :), t, 180/pi*U(2, :), 'LineWidth', 1.5); grid; 
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(2, 1) 180/pi*UAV.ControlBounds(2, 1)], 'r--', 'LineWidth', 1.5);
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(2, 2) 180/pi*UAV.ControlBounds(2, 2)], 'r--', 'LineWidth', 1.5);
        ylim(flip(180/pi*UAV.ControlBounds(2, :)*1.1)); legend('Right Elevon', 'Left Elevon'); 
        ylabel('deg');
%         xticklabels([]);
    otherwise
        subplot(3, 1, 1, 'Units', 'normalized', 'Position', [0.125, 0.65, 0.8, 0.25]);
        hold;
        plot(t, U(4, :), 'LineWidth', 1.5); grid; 
        plot([t(1) t(end)], [UAV.ControlBounds(4, 1) UAV.ControlBounds(4, 1)], 'r--', 'LineWidth', 1.5);
        plot([t(1) t(end)], [UAV.ControlBounds(4, 2) UAV.ControlBounds(4, 2)], 'r--', 'LineWidth', 1.5);
        ylim(flip(UAV.ControlBounds(4, :)*1.1));
        ylabel('%'); legend('Throttle'); 
        xticklabels([]);
        
        subplot(3, 1, 2, 'Units', 'normalized', 'Position', [0.125, 0.40, 0.8, 0.235]);
        hold;
        plot(t, 180/pi*U(1, :), t, 180/pi*U(3, :), 'LineWidth', 1.5); grid; 
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(3, 1) 180/pi*UAV.ControlBounds(3, 1)], 'r--', 'LineWidth', 1.5);
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(3, 2) 180/pi*UAV.ControlBounds(3, 2)], 'r--', 'LineWidth', 1.5);
        ylim(flip(180/pi*UAV.ControlBounds(3, :)*1.1)); legend('Aileron', 'Rudder'); 
        ylabel('deg');
        xticklabels([]);
        
        subplot(3, 1, 3, 'Units', 'normalized', 'Position', [0.125, 0.15, 0.8, 0.235]);
        plot(t, 180/pi*U(2, :), 'LineWidth', 1.5); hold; grid; 
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(2, 1) 180/pi*UAV.ControlBounds(2, 1)], 'r--', 'LineWidth', 1.5);
        plot([t(1) t(end)], [180/pi*UAV.ControlBounds(2, 2) 180/pi*UAV.ControlBounds(2, 2)], 'r--', 'LineWidth', 1.5);
        ylim(flip(180/pi*UAV.ControlBounds(2, :)*1.1)); legend('Elevator'); 
        ylabel('deg');
end
set(findall(gcf,'-property','FontSize'),'FontSize', 12);

xlabel('Time (s)', 'FontSize', 15);
sgtitle('Control Inputs', 'FontSize', 15);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
% return
drawnow;
str = join(['./aerosondeC', string(curveType), 'controls', feed(1:3), string(Noise)], '');
print(str, '-dpdf', '-bestfit', '-r 350');

%%
%Erro de velocidade
dpn = diff(X(1, :))/dt;
dpe = diff(X(2, :))/dt;
dpd = diff(X(3, :))/dt;

t = linspace(0, t_f, size(FInfo.F, 2));
figure;
subplot(3, 1, 1, 'Units', 'normalized', 'Position', [0.1, 0.675, 0.85, 0.235]);
plot(t(1:end-1), dpn, t, FInfo.F(1, :), '--', 'LineWidth', 2); grid;
legend('$\dot{p}_n$', '$F_x$', 'interpreter', 'latex', 'FontSize', 25);
xticklabels([]);
ylabel('m/s');

subplot(3, 1, 2, 'Units', 'normalized', 'Position', [0.1, 0.40, 0.85, 0.235]);
plot(t(1:end-1), dpe, t, FInfo.F(2, :), '--', 'LineWidth', 2); grid;
legend('$\dot{p}_e$', '$F_y$', 'interpreter', 'latex', 'FontSize', 25);
xticklabels([]);
ylabel('m/s');

subplot(3, 1, 3, 'Units', 'normalized', 'Position', [0.1, 0.125, 0.85, 0.235]);
plot(t(1:end-1), dpd, t, -FInfo.F(3, :), '--', 'LineWidth', 2);grid;
legend('$\dot{p}_d$', '$-F_z$', 'interpreter', 'latex', 'FontSize', 25);
ylabel('m/s');

set(findall(gcf,'-property','FontSize'),'FontSize', 12);

xlabel('Time (s)', 'FontSize', 15);
sgtitle('Velocities', 'FontSize', 15);
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
%%

drawnow;
str = join(['./aerosondeC', string(curveType), 'velocities', feed(1:3), string(Noise)], '');
print(str, '-dpdf', '-bestfit', '-r 350');
%%
%Erro de alinhamento com o campo
psi = wrapToPi(atan2(dpe, dpn));
psi(end + 1) = psi(end);
figure;
subplot(2, 1, 1);
plot(t, 180/pi*wrapTo2Pi(psi) - 180/pi*pi, t, 180/pi*wrapTo2Pi(atan2(FInfo.F(2, :), FInfo.F(1, :))) - 180/pi*pi, '--', 'LineWidth', 2);
grid;
legend('$\psi$', '$F_\psi$', 'interpreter', 'latex', 'FontSize', 18);
ylabel('deg');
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
xlabel('Time (s)', 'FontSize', 15);
xticklabels([]);
title('Heading tracking', 'FontSize', 15);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
subplot(2, 1, 2);
plot(t(1:end-1), vecnorm([dpe; dpn; dpd]), t, vecnorm(FInfo.F), '--', 'LineWidth', 2);
legend('$v_t$', '$\|F\|$', 'interpreter', 'latex', 'FontSize', 18);
ylabel('m/s');
xlabel('Time (s)', 'FontSize', 15);
title('Velocity tracking', 'FontSize', 15);
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold'); grid;
%%

drawnow;
str = join(['./aerosondeC', string(curveType), 'heading2pi', feed(1:3), string(Noise)], '');
print(str, '-dpdf', '-bestfit', '-r 350');


%Erro de alinhamento com o campo
psi = wrapToPi(atan2(dpe, dpn));
psi(end + 1) = psi(end);
figure;
subplot(2, 1, 1);
plot(t, 180/pi*wrapToPi(psi), t, 180/pi*wrapToPi(atan2(FInfo.F(2, :), FInfo.F(1, :))), '--', 'LineWidth', 2);
grid;
legend('$\psi$', '$F_\psi$', 'interpreter', 'latex', 'FontSize', 18);
ylabel('deg');
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
xlabel('Time (s)', 'FontSize', 15);
xticklabels([]);
title('Heading tracking', 'FontSize', 15);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
subplot(2, 1, 2);
plot(t(1:end-1), vecnorm([dpe; dpn; dpd]), t, vecnorm(FInfo.F), '--', 'LineWidth', 2);
legend('$v_t$', '$\|F\|$', 'interpreter', 'latex', 'FontSize', 18);
ylabel('m/s');
xlabel('Time (s)', 'FontSize', 15);
title('Velocity tracking', 'FontSize', 15);
set(findall(gcf,'-property','FontSize'),'FontSize', 12);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold'); grid;

drawnow;
str = join(["./aerosondeC", string(curveType), "heading", feed(1:3), string(Noise)], '');
print(str, '-dpdf', '-bestfit', '-r 350');