close all;
clear all;
clc;

set(gcf,'Renderer','opengl')

% load('/home/leonardo/MATLAB/projects/dissertacao/Resultados/Aerosonde/WithNoise/ABV/C2/Result.mat')
load('/home/leonardo/MATLAB/projects/dissertacao/Resultados/Zagi/ZAGI_ABV.mat')
X = Result{1};
FInfo = Result{6};

dt = 0.01;
tf = 200;
t = linspace(0, tf, length(X));

% figure('units','pixels','position',[0 0 1920 1080])

curveType = 2;
[~, ~, ~, O, O_var] = getCurve(curveType, UAV.Type);
h2 = plot_alpha(curveType, 0);
set(gcf, 'position', [0 0 650 650])
% return
hold; 

uav = stlread('fixedwing.stl');
h1 = trimesh(uav, 'FaceColor', 'black', 'EdgeColor', 'blue', 'LineStyle', '-', 'FaceAlpha', 0.5, 'EdgeAlpha', 1);

% xlabel("x_1");
% ylabel('x_2');
% zlabel('x_3');
xticks([]);
yticks([]);
zticks([]);
set(gcf, 'color', 'white');set(gca, 'visible', 'off');
H1 = hgtransform('Parent', gca());
H2 = hgtransform('Parent', gca());
set(h1, 'Parent', H1);
set(h2, 'Parent', H2);
for i=1:size(O, 2)
    x = O(1, i) + 0*O_var{1, i}(0);
    y = O(2, i) + 0*O_var{2, i}(0);
    r = O(4, i);
    %Plota o obstáculo
    ob(i) = plotCylinder(x, y, 300, r, 'black', 0.5);

    %Curva de referencia
    cr(i) = plotCylinder(x, y, 300, r+25, 'yellow', 0.5);

    %Din
    din(i) =plotCylinder(x, y, 300, r+200, 'green', 0.2);
    H3(i) = hgtransform('Parent', gca());
    set(ob(i), 'Parent', H3(i));
    set(cr(i), 'Parent', H3(i));
    set(din(i), 'Parent', H3(i));
end
zlim([100 225]);
xlim([-1200 1200]);
ylim([-1200 1200]);
axis equal;

camproj perspective;
camva(55);

dec = 10;
j = 0;
v = VideoWriter('./ZABVC2H1.avi');
v.Quality = 100;
v.FrameRate = 30;
% set(gcf, 'position', [0 0 650 650])
open(v);
for i=2:dec:length(X)
    if FInfo.state(i) == 1
        h1.EdgeColor = [0 0 1];
    elseif FInfo.state(i) == 3
        h1.EdgeColor = [1 0 0];
    else
        h1.EdgeColor = [0.7 0 0.7];
    end
    
    roll = X(7, i);
    pitch = X(8, i);
    yaw = X(9, i);
    
    x = X(1, i);
    y = X(2, i);
    z = -X(3, i);
    
    dx = x - X(1, i-1);
    dy = y - X(2, i-1);
    
    t1 = makehgtform('translate', [x y z-4], 'scale', 0.5);
    r1 = makehgtform('zrotate', yaw);
    r2 = makehgtform('yrotate', pitch);
    r3 = makehgtform('xrotate', roll);
    set(H1, 'Matrix', t1*r1*r2'*r3');
    t2 = makehgtform('zrotate', 2*pi*i*dt/1200);
    set(H2, 'Matrix', t2);
   
    r = X(1:2, i);
    camtarget([x y z]);
    campos([r + 300*r/norm(r); 300]);
    
    for j=1:size(O, 2)
        tj = makehgtform('translate', [O_var{1, j}(i*dt) O_var{2, j}(i*dt) 0]);
        set(H3(j), 'Matrix', tj);
    end
    
    drawnow;
%     set(gcf, 'position', [0 0 650 650])
    writeVideo(v, getframe(gcf));
    j = j +1;
end

close(v);