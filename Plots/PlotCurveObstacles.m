curveType = 2;
to = 200;
plot_alpha(curveType, to);
[alpha1, alpha2, P, O, O_var, kp] = getCurve(curveType, '');

hold on;
t = 1:15:200;
for i = 1:size(O, 2)
    Ox = O(1, i) + O_var{1, i}(to);
    Oy = O(2, i) + O_var{2, i}(to);
    Or = O(4, i);
    
    ob = plotCylinder(Ox, Oy, 300, Or, 'black', 0.5);
    cr = plotCylinder(Ox, Oy, 300, Or+25, 'yellow', 0.5);
    din = plotCylinder(Ox, Oy, 300, Or+150, 'green', 0.2);
end
ylabel('x_2 (m)');
xlabel('x_1 (m)');
zlabel('x_3 (m)');
zlim([50 250]);
axis equal
grid;
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
set(findall(gcf,'-property','FontSize'),'FontSize',12);