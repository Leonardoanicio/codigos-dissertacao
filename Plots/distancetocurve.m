% close all;
% clear all;
% clc;
% 
% load('/home/leonardo/projects/dissertacao/Resultados/Aerosonde/WithoutNoise/ABV/C1/Result.mat')
% X = Result{1};
% FInfo = Result{6};
dt = 0.01;

curveType = 1;
h2 = plot_alpha(curveType, 0);
C = h2.Vertices';

D = [];
for i = 1:10:size(X, 2)
    t2 = makehgtform('zrotate', 2*pi*i*dt/1200);
   
    pos = X(1:3, i);
    pos(3) = -pos(3);
    d = vecnorm(pos - t2(1:3, 1:3)*C);
    C2 = C;
    C2(1, :) = C2(1, :) - 2*i*dt;
    d = vecnorm(pos - C2);
    D = [D min(d)];
end

close all;
plot(D); %hold; plot(FInfo.D(1:10:end));
