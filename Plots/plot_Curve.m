load('/home/leonardo/projects/dissertacao/Resultados/Zagi/ZAGI_ABV.mat')
X = Result{1};
FInfo = Result{6};

tend = 299;
plot_time = 1;
curveType = 1;
rc = plot_alpha(curveType, tend*plot_time);

tend = round(tend/dt);

hold on;
pn = X(1, 1:tend);
pe = X(2, 1:tend);
pd = X(3, 1:tend);
aa = find(FInfo.state(1:tend) == 1);
bb = find(FInfo.state(1:tend) == 2 | FInfo.state(1:tend) == 4);
cc = find(FInfo.state(1:tend) == 3);

plot3(pn(aa), pe(aa), -pd(aa), '.', 'Color', [0 0 1]);
pa = plot3(0,0,0, 'Color', [0 0 1]);
plot3(pn(bb), pe(bb), -pd(bb), '.', 'Color', [0.7 0 0.7]);
pb = plot3(0,0,0, 'Color', [0.7 0 0.7]);
plot3(pn(cc), pe(cc), -pd(cc), '.', 'Color', [1 0 0]);
pc = plot3(0,0,0, 'Color', [1 0 0]);

% pa = plot3(pn, pe, -pd, 'blue', 'LineWidth', 2);

C = [repmat([0 0 1], length(aa), 1); repmat([0.7 0 0.7], length(bb), 1); repmat([1 0 0], length(cc), 1)];
           

%Pega a curva
[f_a1, f_a2, P, O, O_var, kp] = getCurve(curveType, UAV.Type);

%Plota os obstaculos
begin = false;
angle = linspace(0, 2*pi, 50);
zmin = min(-pd);
zz = zmin*ones(size(angle));
axis([min(pn)-100 max(pn)+100 min(pe)-100 max(pe)+100 min(-pd)-25 max(-pd)+25]);        
for i=1:tend
    if FInfo.state(i) == 3 && begin == false
        near = FInfo.nearo(i);
        
        t = i*dt*plot_time;
        Oi = O(:, near);
        O_vari = [];
        
        for n = near
            O_vari = [O_vari, [O_var{1, n}(t); O_var{2, n}(t)]];
        end
        
        x1 = Oi(1, :) + O_vari(1, :);
        y1 = Oi(2, :) + O_vari(2, :);
        r = Oi(4);

        begin = true;
    elseif FInfo.state(i) == 4 && begin == true
        begin = false;
        
        near = FInfo.nearo(i);
        
        t = i*dt*plot_time;
        Oi = O(:, near);
        O_vari = [];
        
        for n = near
            O_vari = [O_vari, [O_var{1, n}(t); O_var{2, n}(t)]];
        end
        
        x2 = Oi(1, :) + O_vari(1, :);
        y2 = Oi(2, :) + O_vari(2, :);
        r = Oi(4);
        
        Xo = x1 + (x2 - x1)/2;
        Yo = y1 + (y2 - y1)/2;
        
        for j=1:length(Xo)
            x = Xo(j);
            y = Yo(j);
            %Plota o obstáculo
            ob = plotCylinder(x, y, 300, r, 'black', 0.5);

            %Curva de referencia
            cr = plotCylinder(x, y, 300, r+25, 'yellow', 0.5);

            %Din
            din =plotCylinder(x, y, 300, r+150, 'green', 0.2);
        end
    end
end

axis equal
xlim([min(pn)-325 max(pn)+100])
ylim([min(pe)-100 max(pe)+100])
zlim([-max(pd)-25 -min(pd)+25])

xlabel('x_1 (m)'); ylabel('x_2 (m)'); zlabel('x_3 (m)');
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
set(findall(gcf,'-property','FontSize'),'FontSize',12);
h = legend([rc, ob, cr, din, pa, pb, pc], 'Target Curve', 'Obstacle', 'Obstacle Curve', '$D_{in}$', 'Follow Curve', 'Transition', 'Contour Obstacle',...
    'interpreter', 'latex', 'FontSize', 12, 'NumColumns', 3, 'Orientation', 'Horizontal', 'Location', 'South');
% legend([rc, ob, cr, din, pa], 'Target Curve', 'Obstacle', 'Obstacle Curve', '$D_{in}$', 'UAV Trajectory', 'interpreter', 'latex', 'FontSize', 12)