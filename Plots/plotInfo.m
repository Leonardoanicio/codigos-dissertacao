function [Figs] = plotInfo(X, U, E, Re, Tout, FInfo, Wind, LinSys, t, curveType, finalPlot, animate, finalVideo)
    [fC, O] = getParamCurve(curveType);

    pn = X(1,:);
    pe = X(2,:);
    pd = X(3,:);
    u = X(4,:);
    v = X(5,:);
    w = X(6,:);
    phi = X(7,:);
    theta = X(8,:);
    psi = wrapToPi(X(9,:));
    p = X(10,:);
    q = X(11,:);
    r = X(12, :);
    
    pen = LinSys.noise(1, :) + pe;
    pnn = LinSys.noise(2, :) + pn;
    pdn = LinSys.noise(3, :) + pd;
    phin = LinSys.noise(4, :) + phi;
    thetan = LinSys.noise(5, :) + theta;
    psin = LinSys.noise(6, :) + psi;

    noise = 1;
    
    R = @(phi, theta, psi)[
                cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
                sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
                -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
                ];
    
    ur = u;
    vr = v;
    wr = w;
    for i=1:length(psi)
        wind = R(phi(i), theta(i), psi(i))'*Wind;
        ur(i) = ur(i) - wind(1);
        vr(i) = vr(i) - wind(2);
        wr(i) = wr(i) - wind(3);
    end
    
    vt = vecnorm([ur; vr; wr]);
    alpha = atan2(wr, ur);
    beta = asin(vr./vt);

    pos = [pe; pn; -pd];

    if finalPlot
        %Plota os controles
        fU = figure();
        subplot(2, 2, 1);
        plot(t, U(1, :), 'LineWidth', 2);        legend('Aileron');
            grid on;
        subplot(2, 2, 2);
        plot(t, U(2, :), 'LineWidth', 2);        legend('Elevator');
            grid on;
        subplot(2, 2, 3);
        plot(t, U(3, :), 'LineWidth', 2);        legend('Rudder');
            grid on;
        subplot(2, 2, 4);
        plot(t, U(4, :), 'LineWidth', 2);        legend('Throttle');
            grid on;
        sgtitle('Controles');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
        
        %Plot as velocidades
        fVel = figure();
        subplot(2, 2, 1);
        plot(t, vt, 'LineWidth', 2);              legend('v_{true}');
            grid on;
        subplot(2, 2, 2);
        plot(t, ur, 'LineWidth', 2);               legend('u_r');
            grid on;
        subplot(2, 2, 3);
        plot(t, vr, 'LineWidth', 2);               legend('v_r');
            grid on;
        subplot(2, 2, 4);
        plot(t, wr, 'LineWidth', 2);               legend('w_r');
            grid on;
        sgtitle('Velocidades');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
        
        
        
        fErr = figure();
        subplot(3, 2, 1);
        plot(t, E(1, :), 'LineWidth', 2);              legend('\alpha Error');
        grid on;
        subplot(3, 2, 2);
        plot(t, E(2, :), 'LineWidth', 2);               legend('\beta Error');
        grid on;
        subplot(3, 2, 3);
        plot(t, E(3, :), 'LineWidth', 2);               legend('\v_{true} Error');
        grid on;
        subplot(3, 2, 4);
        plot(t, E(4, :), 'LineWidth', 2);               legend('\dot{\phi} Error');
        grid on;
        subplot(3, 2, 5);
        plot(t, E(5, :), 'LineWidth', 2);               legend('\dot{\theta} Error');
        grid on;
        subplot(3, 2, 6);
        plot(t, E(6, :), 'LineWidth', 2);               legend('\dot{\psi} Error');
        grid on;
        sgtitle('Erros');
        set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        grid on;
        drawnow;
        
        
        
        
        if noise
            %Plot as posiçoes
            fPos = figure();
            subplot(3, 2, 1);
            plot(t, pe, 'LineWidth', 2);              legend('Real East');
            grid on;
            subplot(3, 2, 2);
            plot(t, pen, 'LineWidth', 2);               legend('Noisy East');
            grid on;
            subplot(3, 2, 3);
            plot(t, pn, 'LineWidth', 2);               legend('Real North');
            grid on;
            subplot(3, 2, 4);
            plot(t, pnn, 'LineWidth', 2);               legend('Noisy North');
            grid on;
            subplot(3, 2, 5);
            plot(t, pd, 'LineWidth', 2);               legend('Real Down');
            grid on;
            subplot(3, 2, 6);
            plot(t, pdn, 'LineWidth', 2);               legend('Noisy Down');
            grid on;
            sgtitle('Posições');
            set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
            set(findall(gcf,'-property','FontSize'),'FontSize',12);
            grid on;
            drawnow;
            
            %Plot as orientações
            fTheta = figure();
            subplot(3, 2, 1);
            plot(t, phi, 'LineWidth', 2);              legend('Real Roll');
            grid on;
            subplot(3, 2, 2);
            plot(t, phin, 'LineWidth', 2);               legend('Noisy Roll');
            subplot(3, 2, 3);
            plot(t, theta, 'LineWidth', 2);               legend('Real Pitch');
            grid on;
            grid on;
            subplot(3, 2, 4);
            plot(t, thetan, 'LineWidth', 2);               legend('Noisy Pitch');
            grid on;
            subplot(3, 2, 5);
            plot(t, psi, 'LineWidth', 2);               legend('Real Yaw');
            grid on;
            subplot(3, 2, 6);
            plot(t, psin, 'LineWidth', 2);               legend('Noisy Yaw');
            grid on;
            sgtitle('Posições');
            set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
            set(findall(gcf,'-property','FontSize'),'FontSize',12);
            drawnow;
        end
        
        
        %Plota a curva com os dados
        fTraj = figure('units','normalized','outerposition',[0 0 1 1]);
        hold on;
        aa = find(FInfo.state == 1);
        bb = find(FInfo.state == 2 | FInfo.state == 4);
        cc = find(FInfo.state == 3);
        plot3(pe(aa), pn(aa), -pd(aa), '.', 'Color', [0 0 1]);
        pa = plot3(0,0,0, 'Color', [0 0 1]);
        plot3(pe(bb), pn(bb), -pd(bb), '.', 'Color', [0.7 0 0.7]);
        pb = plot3(0,0,0, 'Color', [0.7 0 0.7]);
        plot3(pe(cc), pn(cc), -pd(cc), '.', 'Color', [1 0 0]);
        pc = plot3(0,0,0, 'Color', [1 0 0]);
        
        C = fC(0:0.01:2*pi);
        rc = plot3(C(1, :), C(2, :), C(3, :), 'k');
        zmin = min(-pd);
        zmax = max(-pd);
        angle = linspace(0, 2*pi, 50);
        zz = zmin*ones(size(angle));
        
        %Plota os obstaculos
        for k = 1:size(O, 2)
            Oi = O(:,k);
            x = Oi(1);
            y = Oi(2);
            r = Oi(4);
            
            %Plota o obstáculo
            xx = x + r*cos(angle);
            yy = y + r*sin(angle);
            plot3(xx, yy, zz, 'Color', [0 0 0]);

            %Curva de referencia
            xx = x + (r + 50)*cos(angle);
            yy = y + (r + 50)*sin(angle);
            plot3(xx, yy, zz, 'Color', [1 1 0]);

            %Din
            xx = x + (r + 150)*cos(angle);
            yy = y + (r + 150)*sin(angle);
            plot3(xx, yy, zz, 'Color', [0 1 0]);
        end
        axis equal;
        legend([pa, pb, pc, rc], 'Follow Curve', 'Transition', 'Contour Obstacle', 'Curve');
        drawnow;
        
        
        
        if animate
            fAnimation = figure('units','normalized','outerposition',[0 0 1 1]);
            figure(fAnimation);
            plot3(C(1, :), C(2, :), C(3, :), 'k');
            hold on;
            drawnow;
            POB = [];
            angle = linspace(0, 2*pi, 50);
            zz = zmin*ones(size(angle));
            for k = 1:size(O, 2)
                Oi = O(:,k);
                x = Oi(1);
                y = Oi(2);
                r = Oi(4);

                %Plota o obstáculo
                xx = x + r*cos(angle);
                yy = y + r*sin(angle);
                POB = [POB plot3(xx, yy, zz, '--', 'Color', [0 0 0])];

                %Curva de referencia
                xx = x + (r + 50)*cos(angle);
                yy = y + (r + 50)*sin(angle);
                rc = plot3(xx, yy, zz, '--', 'Color', [1 1 0]);

                %Din
                xx = x + (r + 150)*cos(angle);
                yy = y + (r + 150)*sin(angle);
                din = plot3(xx, yy, zz, '--', 'Color', [0 1 0]);
            end

            hold on;
            if finalVideo
                V = VideoWriter('trifolio.avi');
                V.Quality = 75;
                V.FrameRate = 30;
                open(V);
            end
            p1 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 10, 'MarkerFaceColor', 'blue'); 
            p2 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'red'); 
            f1 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'green', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            f2 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'blue', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            f3 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'red', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
            
            axis equal
            for t=1:25:length(pe)
                if FInfo.state(t) == 1
                    p1.MarkerFaceColor = [0 0 1];
                elseif FInfo.state(t) == 3
                    p1.MarkerFaceColor = [1 0 0];
                else
                    p1.MarkerFaceColor = [0.7 0 0.7];
                end
                
                f = FInfo.F(:, t);
                fc = FInfo.Fc(:, t);
                fo = FInfo.Fo(:, t);
                
                f = 50*f/(1e-3 + norm(f));
                fc = 50*fc/(1e-3 + norm(fc));
                fo = 50*fo/(1e-3 + norm(fo));
                
                p1.XData = pe(t);
                p1.YData = pn(t);
                p1.ZData = -pd(t);
                
                p2.XData = FInfo.nearp(1, t);
                p2.YData = FInfo.nearp(2, t);
                p2.ZData = FInfo.nearp(3, t);
                
                f1.XData = pe(t); f1.YData = pn(t); f1.ZData = -pd(t);
                f2.XData = pe(t); f2.YData = pn(t); f2.ZData = -pd(t);
                f3.XData = pe(t); f3.YData = pn(t); f3.ZData = -pd(t);
                
                f1.UData = f(1); f1.VData = f(2); f1.WData = 1;
                f2.UData = fc(1); f2.VData = fc(2); f2.WData = 1;
                f3.UData = fo(1); f3.VData = fo(2); f3.WData = 1;
                
                axis([min(pe)-50 max(pe)+50 min(pn)-50 max(pn)+50]);
                view([0 90]);
                legend([p1, p2, f1, f2, f3, POB(1), rc, din],'UAV', 'Closest Point', ...
                    'Total Field', 'Curve Field', 'Obstacle Field',...
                     'Obstacles','Safe Zone', 'D_{in}');
                drawnow;

                for pob = POB
                    pob.Color = [0 0 0];
                end

                if FInfo.state(t) >= 2
                    POB(FInfo.nearo(t)).Color = [1 0 0];
                end
                if finalVideo
                    writeVideo(V, getframe(gcf));
                end
            end
            figure(fTraj);
            drawnow;
            if finalVideo
                for i=1:V.FrameRate*2
                    writeVideo(V, getframe(gcf));
                end
            end
            close(V);
        end
    end
    set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
    set(findall(gcf,'-property','FontSize'),'FontSize',12);
    grid on;
    if animate
        Figs = [fTraj, fAnimation, fU, fVel];
    else
        Figs = [fTraj, fU, fVel];
    end
end