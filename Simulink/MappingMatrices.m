function [Coeffs_M, Coeffs_C, Coeffs_P] = MappingMatrices(alpha, beta, vt, p, q, r, throttle, Aero)
    qbar = (1/2)*Aero.rho*vt^2;
            
    %Lineariza throttle^2
    b = -throttle^2;
    a = 2*throttle;

    Coeffs_C = coeffs_C(Aero, qbar, alpha, beta, vt, p, q, r, b);
    Coeffs_M = coeffs_M(Aero, qbar, alpha, vt, a);
    Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
end

function C_6x1 = coeffs_C(Aero, qbar, alpha, beta, vt, p, q, r, fx_throttle)
    sigma = (1 + exp(-Aero.M*(alpha - Aero.alpha0)) + exp(Aero.M*(alpha + Aero.alpha0)))/...
            ((1 + exp(-Aero.M*(alpha - Aero.alpha0)))*(1 + exp(Aero.M*(alpha + Aero.alpha0))));
    CL = (1 - sigma)*(Aero.CL0 + Aero.CLalpha*alpha) + sigma*(2*sign(alpha)*sin(alpha)^2*cos(alpha));
    CD = Aero.CDp + (Aero.CL0 + Aero.CLalpha*alpha)/(pi*Aero.e*Aero.AR);

    Cxalpha = -CD*cos(alpha) + CL*sin(alpha);
    Cxq = -Aero.CDq*cos(alpha) + Aero.CLq*sin(alpha);
    Czalpha = -CD*sin(alpha) - CL*cos(alpha);
    Czq = -Aero.CDq*sin(alpha) - Aero.CLq*cos(alpha);


    fx = qbar*Aero.S*(Cxalpha + Cxq*Aero.c*q/(2*vt)) + qbar/(vt^2)*Aero.Sprop*Aero.Cprop*(Aero.Kmotor^2*fx_throttle - vt^2);
    fy = qbar*Aero.S*(Aero.CY0 + Aero.CYbeta*beta + Aero.CYp*Aero.b*p/(2*vt) + Aero.CYr*Aero.b*r/(2*vt));
    fz = qbar*Aero.S*(Czalpha + Czq*Aero.c*q/(2*vt));
    l = qbar*Aero.S*(Aero.b*(Aero.Cl0 + Aero.Clbeta*beta + Aero.Clp*Aero.b*p/(2*vt) + Aero.Clr*Aero.b*r/(2*vt)));
    m = qbar*Aero.S*(Aero.c*(Aero.Cm0 + Aero.Cmalpha*alpha + Aero.Cmq*Aero.c*q/(2*vt)));
    n = qbar*Aero.S*(Aero.b*(Aero.Cn0 + Aero.Cnbeta*beta + Aero.Cnp*Aero.b*p/(2*vt) + Aero.Cnr*Aero.b*r/(2*vt)));
    C_6x1 = [fx; fy; fz; l; m; n];
end

function M_6x4 = coeffs_M(Aero, qbar, alpha, vt, fx_throttle)
    Cxde = -Aero.CDde*cos(alpha) + Aero.CLde*sin(alpha);
    Czde = -Aero.CDde*sin(alpha) - Aero.CLde*cos(alpha);

    fx = [0 qbar*Aero.S*Cxde 0 qbar*Aero.Sprop*Aero.Cprop*Aero.Kmotor^2*fx_throttle/(vt^2)-...
                                    Aero.Ktp*Aero.Komega^2*fx_throttle];
    fy = [qbar*Aero.S*Aero.CYda 0 qbar*Aero.S*Aero.CYdr 0];
    fz = [0 qbar*Aero.S*Czde 0 0];
    l = [qbar*Aero.S*Aero.b*Aero.Clda 0 qbar*Aero.S*Aero.b*Aero.Cldr 0];
    m = [0 qbar*Aero.S*Aero.c*Aero.Cmde 0 0];
    n = [qbar*Aero.S*Aero.b*Aero.Cnda 0 qbar*Aero.S*Aero.b*Aero.Cndr 0];

    M_6x4 = [fx; fy; fz; l; m; n];
end
        
     
        