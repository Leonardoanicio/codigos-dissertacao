close all
clear
clc

%
load('bus');

%Simulacao
dt = 0.01;
tf = 200;

%MPC
nu = 2; 
nx = 10;

%Escolhe o campo
curveType = 1;

%%%%%%
%Escolhe o feedback
feedType = "ABVEULdot"; %ABVEULdot gives the best results (til now?)

[Hqp, Fqp, S, T, options] = MPCInitialization(nx, nu, dt, feedType);
MPC.Hqp = Hqp;
MPC.Fqp = Fqp;
MPC.S = S;
MPC.T = T;
MPC.nx = nx;
MPC.nu = nu;
MPC.options = options;

%Estados iniciais
states = [500 150 -200 12 0 1 0 0.05 -pi/2 0 0 0]';
controls = [0 0 0 0.5]';

%Vento
Wind = [0; 0; 0]; %WIND IN NED FRAME
Winddot = [0; 0; 0];

%Seleciona o VANT
UAV = AerosondeBeard(states, controls);

global ControlBounds StateBounds
ControlBounds = UAV.ControlBounds;
StateBounds = UAV.StateBounds;

%Trimming inicial
[UAV, xdot] = trim(UAV, Wind, Winddot, dt);
states = cell2mat(UAV.States);
controls = UAV.Controls;

%Estados iniciais dos integradores
NED_0 = states(1:3);
uvw_0 = states(4:6);
eul_0 = states(7:9);
pqr_0 = states(10:12);

control0 = struct('Aileron', controls(1),...
                'Elevator', controls(2),...
                 'Rudder', controls(3),...
                'Throttle', controls(4));