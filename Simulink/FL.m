function [Alpha, Beta] = FL(ur, vr, wr, phi, theta, psi, derivatives, C, UAV)    
    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([ur, vr, wr]);
    alpha = atan2(wr, ur);
    beta = asin(vr/vt);
    
    dphi = derivatives(1);
    dtheta = derivatives(2);
    dpsi = derivatives(3);
    
    wdowndot = 0;
    wnorthdot = 0;
    weastdot = 0;
    
    g = 9.81;
    
    Delta = [
        -sin(alpha) / vt / cos(beta) / UAV.uavMass 0 cos(alpha) / vt / cos(beta) / UAV.uavMass 0 0 0;
        -(cos(beta) ^ 2 * vt ^ 2) ^ (-0.1e1 / 0.2e1) * cos(alpha) * sin(beta) * cos(beta) / UAV.uavMass (cos(beta) ^ 2 * vt ^ 2) ^ (-0.1e1 / 0.2e1) * cos(beta) ^ 2 / UAV.uavMass -(cos(beta) ^ 2 * vt ^ 2) ^ (-0.1e1 / 0.2e1) * sin(alpha) * sin(beta) * cos(beta) / UAV.uavMass 0 0 0;
        cos(alpha) * cos(beta) / UAV.uavMass sin(beta) / UAV.uavMass sin(alpha) * cos(beta) / UAV.uavMass 0 0 0;
        0 0 0 0.1e1 / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * (UAV.Jxz * sin(theta) * cos(phi) + UAV.Jz * cos(theta)) / cos(theta) sin(theta) * sin(phi) / cos(theta) / UAV.Jy 0.1e1 / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * (UAV.Jx * sin(theta) * cos(phi) + UAV.Jxz * cos(theta)) / cos(theta);
        0 0 0 -sin(phi) * UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) cos(phi) / UAV.Jy -sin(phi) * UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        0 0 0 UAV.Jxz * cos(phi) / cos(theta) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) sin(phi) / cos(theta) / UAV.Jy UAV.Jx * cos(phi) / cos(theta) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ];


    B = [
        ((UAV.uavMass * cos(phi) * (g - wdowndot) * cos(theta) - UAV.uavMass * cos(phi) * (weastdot * sin(psi) + wnorthdot * cos(psi)) * sin(theta) + UAV.uavMass * vt * sin(beta) * sin(theta) * dpsi - UAV.uavMass * vt * dphi * sin(beta) + UAV.uavMass * (weastdot * cos(psi) - wnorthdot * sin(psi)) * sin(phi) + fz_ex) * cos(alpha) + (UAV.uavMass * (-vt * sin(beta) * dpsi * cos(phi) + weastdot * sin(psi) + wnorthdot * cos(psi)) * cos(theta) + UAV.uavMass * (g - wdowndot) * sin(theta) + dtheta * UAV.uavMass * sin(phi) * vt * sin(beta) - fx_ex) * sin(alpha) + vt * UAV.uavMass * cos(beta) * (sin(phi) * dpsi * cos(theta) + dtheta * cos(phi))) / vt / UAV.uavMass / cos(beta);
        -cos(beta) * (((UAV.uavMass * cos(phi) * (g - wdowndot) * cos(theta) - UAV.uavMass * cos(phi) * (weastdot * sin(psi) + wnorthdot * cos(psi)) * sin(theta) + UAV.uavMass * (weastdot * cos(psi) - wnorthdot * sin(psi)) * sin(phi) + fz_ex) * sin(alpha) - cos(alpha) * (UAV.uavMass * (weastdot * sin(psi) + wnorthdot * cos(psi)) * cos(theta) + UAV.uavMass * (g - wdowndot) * sin(theta) - fx_ex)) * sin(beta) + UAV.uavMass * vt * (sin(theta) * dpsi - dphi) * sin(alpha) + (-UAV.uavMass * sin(phi) * (g - wdowndot) * cos(theta) + UAV.uavMass * sin(phi) * (weastdot * sin(psi) + wnorthdot * cos(psi)) * sin(theta) + UAV.uavMass * (weastdot * cos(psi) - wnorthdot * sin(psi)) * cos(phi) - fy_ex) * cos(beta) - UAV.uavMass * vt * cos(alpha) * (-cos(phi) * cos(theta) * dpsi + dtheta * sin(phi))) * (cos(beta) ^ 2 * vt ^ 2) ^ (-0.1e1 / 0.2e1) / UAV.uavMass;
        (((UAV.uavMass * cos(phi) * (g - wdowndot) * cos(theta) - UAV.uavMass * cos(phi) * (weastdot * sin(psi) + wnorthdot * cos(psi)) * sin(theta) + UAV.uavMass * (weastdot * cos(psi) - wnorthdot * sin(psi)) * sin(phi) + fz_ex) * sin(alpha) - cos(alpha) * (UAV.uavMass * (weastdot * sin(psi) + wnorthdot * cos(psi)) * cos(theta) + UAV.uavMass * (g - wdowndot) * sin(theta) - fx_ex)) * cos(beta) + (UAV.uavMass * sin(phi) * (g - wdowndot) * cos(theta) - UAV.uavMass * sin(phi) * (weastdot * sin(psi) + wnorthdot * cos(psi)) * sin(theta) - UAV.uavMass * (weastdot * cos(psi) - wnorthdot * sin(psi)) * cos(phi) + fy_ex) * sin(beta)) / UAV.uavMass;
        (-0.2e1 * (UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * cos(theta) * UAV.Jxz * sin(theta) * dpsi * dtheta * cos(phi) ^ 3 + ((-(UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * UAV.Jxz * sin(theta) * dpsi * sin(phi) + ((UAV.Jx - UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx ^ 2 + (-UAV.Jy - UAV.Jz) * UAV.Jx + 0.2e1 * UAV.Jy * UAV.Jz)) * dtheta) * dpsi * cos(theta) ^ 2 + ((UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * UAV.Jxz * sin(theta) * dtheta * sin(phi) + (sin(theta) * dphi - dpsi) * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz))) * dtheta) * cos(phi) ^ 2 + ((UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * (UAV.Jx - UAV.Jz) * dpsi ^ 2 * sin(phi) * cos(theta) ^ 3 + ((dphi * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz)) * dpsi * sin(theta) + ((-UAV.Jx - UAV.Jy + UAV.Jz) * UAV.Jxz ^ 2 - UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz)) * dpsi ^ 2 - UAV.Jy * dtheta ^ 2 * (-UAV.Jxz ^ 2 + UAV.Jz * (UAV.Jy - UAV.Jz))) * sin(phi) - 0.2e1 * UAV.Jxz * dtheta * (dpsi * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(theta) - UAV.Jy * dphi * (UAV.Jx - UAV.Jy + UAV.Jz) / 0.2e1)) * cos(theta) + sin(theta) * UAV.Jy * (UAV.Jx * n_ex + UAV.Jxz * l_ex)) * cos(phi) + (-UAV.Jxz * ((UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * dpsi * sin(theta) - (0.2e1 * UAV.Jxz ^ 2 + (UAV.Jy - 0.2e1 * UAV.Jz) * UAV.Jx - UAV.Jy * (UAV.Jy - UAV.Jz)) * dphi) * sin(phi) + ((-UAV.Jx + UAV.Jy + UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jz * (UAV.Jx - UAV.Jy) * (UAV.Jx + UAV.Jy - UAV.Jz)) * dtheta) * dpsi * cos(theta) ^ 2 + UAV.Jy * (UAV.Jxz * n_ex + UAV.Jz * l_ex) * cos(theta) + (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * (((-UAV.Jxz * dpsi ^ 2 + (-dphi ^ 2 + dtheta ^ 2) * UAV.Jxz + m_ex) * sin(theta) + 0.2e1 * UAV.Jxz * dphi * dpsi) * sin(phi) + (dphi * (UAV.Jx + UAV.Jy - UAV.Jz) * sin(theta) - dpsi * (UAV.Jx - UAV.Jy - UAV.Jz)) * dtheta)) / cos(theta) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / UAV.Jy;
        ((cos(theta) * dpsi + dtheta) * (UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * UAV.Jxz * (-cos(theta) * dpsi + dtheta) * cos(phi) ^ 3 - cos(theta) * (-0.2e1 * (UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * UAV.Jxz * dtheta * sin(phi) + (sin(theta) * dpsi - dphi) * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz))) * dpsi * cos(phi) ^ 2 + ((-UAV.Jxz ^ 2 + (UAV.Jy + UAV.Jz) * UAV.Jx - UAV.Jy * (UAV.Jy - UAV.Jz)) * UAV.Jxz * dpsi ^ 2 * cos(theta) ^ 2 + (sin(theta) * dpsi - dphi) * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz)) * dtheta * sin(phi) + 0.2e1 * UAV.Jxz * dphi * dpsi * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(theta) + (dphi ^ 2 + dpsi ^ 2 - dtheta ^ 2) * UAV.Jxz ^ 3 - m_ex * UAV.Jxz ^ 2 + (-UAV.Jx * UAV.Jz * dpsi ^ 2 + (-UAV.Jy * dtheta ^ 2 - UAV.Jz * dphi ^ 2 + UAV.Jz * dtheta ^ 2) * UAV.Jx + dtheta ^ 2 * UAV.Jy * (UAV.Jy - UAV.Jz)) * UAV.Jxz + UAV.Jx * UAV.Jz * m_ex) * cos(phi) + UAV.Jy * (dpsi * (-dtheta * UAV.Jxz * (UAV.Jx - UAV.Jy + UAV.Jz) * sin(phi) + ((UAV.Jx - UAV.Jy) * UAV.Jx + UAV.Jxz ^ 2) * dpsi * sin(theta) - UAV.Jx * dphi * (UAV.Jx - UAV.Jy + UAV.Jz)) * cos(theta) - sin(phi) * (UAV.Jx * n_ex + UAV.Jxz * l_ex))) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / UAV.Jy;
        (-0.2e1 * (UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * cos(theta) * UAV.Jxz * dpsi * dtheta * cos(phi) ^ 3 + ((cos(theta) * dpsi + dtheta) * (UAV.Jxz ^ 2 + (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy)) * UAV.Jxz * (-cos(theta) * dpsi + dtheta) * sin(phi) - (sin(theta) * dpsi - dphi) * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz)) * dtheta) * cos(phi) ^ 2 + (-cos(theta) * (sin(theta) * dpsi - dphi) * ((UAV.Jx + UAV.Jy - UAV.Jz) * UAV.Jxz ^ 2 + UAV.Jx * (UAV.Jy - UAV.Jz) * (UAV.Jx - UAV.Jy - UAV.Jz)) * dpsi * sin(phi) + UAV.Jxz * (0.2e1 * UAV.Jxz ^ 2 + (UAV.Jy - 0.2e1 * UAV.Jz) * UAV.Jx - UAV.Jy * (UAV.Jy - UAV.Jz)) * dpsi * dtheta * cos(theta) + UAV.Jy * (UAV.Jx * n_ex + UAV.Jxz * l_ex)) * cos(phi) - (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * ((-UAV.Jxz * dpsi ^ 2 * cos(theta) ^ 2 - 0.2e1 * UAV.Jxz * dphi * dpsi * sin(theta) + (dphi ^ 2 + dpsi ^ 2 - dtheta ^ 2) * UAV.Jxz - m_ex) * sin(phi) + (dpsi * (UAV.Jx - UAV.Jy - UAV.Jz) * sin(theta) - dphi * (UAV.Jx + UAV.Jy - UAV.Jz)) * dtheta)) / cos(theta) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / UAV.Jy;
        ];

    Beta = inv(Delta);
    Alpha = -Beta*B;
end