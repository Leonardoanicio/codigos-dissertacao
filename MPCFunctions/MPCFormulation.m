function [Hqp, Fqp, Qb, T, S] = MPCFormulation(A, B, Q, R, P, nx, nu)

Q2 = [];
R2 = [];
S = [];
T = [];

nx = double(nx);
nu = double(nu);

for i=1:nx
    linha = [];
    for j=1:nu
        expMin = i - j;
        expMax = i - j;
        elementS = zeros(size(B));
        
        if i > nu && j == nu
            expMin = 0;
        end
        
        if expMax >= expMin && expMin >= 0
            for expA = expMin:expMax
                elementS = elementS + A^expA*B;
            end
        end
        
        linha = [linha elementS];
    end
    S = [S; linha];
    
    if i <= nu
        R2 = blkdiag(R2, R);
    end
    if i ~= nx
        Q2 = blkdiag(Q2, Q);
    else
        Q2 = blkdiag(Q2, P);
    end
    
    T = [T; A^i];
end

Q = Q2;
R = R2;
Qb = Q;

Hqp = 2*(R + S'*Q*S);
Hqp = (Hqp' + Hqp)/2;
Fqp = Q*S;
end