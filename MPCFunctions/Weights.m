function [A, B, Q, R, P] = Weights(feedType, dt, UAV)
    
    switch feedType
        case "ABVEULdot"
            A = eye(6);
            B = eye(6)*dt;
            switch UAV.Type
                case 'elevon'
                    Q = diag([1 1 1 10 10 10]);
                    R = diag([1 1 1 1 1 1]);
                otherwise
%                     Q = diag([1 1 1 15 20 15]);
                    Q = diag([1 1 1 10 15 10]);
%                     Q = diag([1 1 1 10 20 10]);
%                     Q = diag([1 1 1 5 5 5]);
%                     R = eye(6)*0.1;
                    R = diag([1 1 1 5 5 5]);
            end
        case "NEDdotEULdot"
            A = eye(6);
            B = eye(6)*dt;
            switch UAV.Type
                case 'elevon'
                    Q = diag([1 1 1 2.5 2.5 2.5]);
                    R = diag([10 10 10 1 1 1]);
                otherwise
                    Q = diag([5 5 15 30 30 30]);
                    R = diag([1 1 1 1 1 1]);
            end
    end
    Q = blkdiag(Q, zeros(size(R)));
    A = [A B; zeros(size(B')) eye(size(B, 2))];
    B = [B; eye(size(B, 2))];
    
    [P, K] = idare(A,B,Q,R); 
end