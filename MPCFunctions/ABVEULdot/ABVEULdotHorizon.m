function [horizon, stateOut, BetaOut, circOut, obstOut, D, Fout, fcOut, foOut, phiref, psiref, thetaref] = ABVEULdotHorizon(states, nx, dt,...
                                                                state, tgamma, circDir, curveType, time, Wind, aux, UAV)
    horizon = zeros(12*nx, 1);
    
    k_psi = 1.5;
    k_theta = 1;
    k_phi = 1.5;
    
    [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    h = -pd;
    
    alpha = atan2(w, u);
    vt = norm([u; v; w]);
    beta = asin(v/vt);
    
    pos = [pn pe h]';
    
    [Fout, D, stateOut, BetaOut, circOut, obstOut, fcOut, foOut, Tbeta] = BugFieldAlpha(pos, curveType, state, tgamma, circDir, time, UAV.Type);  
    [F1, ~, FState, Tgamma, circDir] = BugFieldAlpha(pos, curveType, state, tgamma, circDir, time, UAV.Type);  
   
    psiref = zeros(nx + 1, 1);
    dpsiref = zeros(nx + 1, 1);
    thetaref = zeros(nx + 1, 1);
    phiref = zeros(nx + 1, 1);
    vtref = zeros(nx + 1, 1);
    betaref = zeros(nx + 1, 1);
    
    pos = pos + F1*dt;
    statevec = [FState];
    for i=1:nx+1
        [F1, ~, ~, Tgamma, circDir] = BugFieldAlpha(pos, curveType, FState, Tgamma, circDir, time + i*dt, UAV.Type);      
        if norm(F1(1:2)) == 0
            [F1, ~, ~, Tgamma, circDir] = BugFieldAlpha(pos + [sin(psi); cos(psi); 0], curveType, FState, Tgamma, circDir, time + i*dt, UAV.Type);  
        end
                
        statevec = [statevec; FState];
        
        pos2 = pos + F1*dt;
        F2 = BugFieldAlpha(pos2, curveType, FState, Tgamma, circDir, time + i*dt + dt, UAV.Type);      
        if norm(F2(1:2)) == 0
            F2 = BugFieldAlpha(pos2 + [sin(psi); cos(psi); 0], curveType, FState, Tgamma, circDir, time + i*dt + dt, UAV.Type);  
        end
        
        F1(3) = -F1(3);
        F1 = F1 - Wind;
        F2(3) = -F2(3);
        F2 = F2 - Wind;
        
        Fn = F1(1);
        Fe = F1(2);
        Fd = F1(3);
        
        Vd = Fd;
        
        Vtref = norm(F1);
        
        %%
        betaref(i) = 0;
        
        %%
        F_psi = atan2(Fe, Fn);
        F_psi2 = atan2(F2(2), F2(1));
        psivec = unwrap([psi F_psi F_psi2]);
        dpsi = k_psi*(psivec(2) - psivec(1)) + 0.25*(psivec(3)-psivec(2))/dt;
        
        gamma = asin(-Vd/Vtref);
        g = dpsi*Vtref/9.81;
        
        a = 1 - g*tan(alpha)*sin(beta);
        b = sin(gamma)/cos(beta);
        c = 1 + g^2*cos(beta)^2;        
        tanphi = (g*cos(beta)*(a-b^2) + b*tan(alpha)*(c*(1 - b^2) + g^2*sin(beta)^2)^(1/2))/(cos(alpha)*(a^2 - b^2*(1 + c*tan(alpha)^2)));
        F_phi = atan(tanphi);

        a = cos(alpha)*cos(beta);
        b = sin(F_phi)*sin(beta) + cos(F_phi)*sin(alpha)*cos(beta);
        F_theta = (a*b + sin(gamma)*sqrt(a^2 - sin(gamma)^2 + b^2))/(a^2 - sin(gamma)^2);
        F_theta = atan(F_theta);
        
        F_theta = gamma + UAV.alphass;
        F_psi = g;
        
        psi = psi + dpsi*dt;
        pos = pos + [F1(1); F1(2); -F1(3)]*dt;
         
        vtref(i) = Vtref;
        dpsiref(i) = dpsi;
        psiref(i) = F_psi;
        phiref(i) = F_phi;
        thetaref(i) = F_theta;
    end    
    
    thetadot = diff(thetaref)/dt;
    phidot = diff(phiref)/dt;
    
    for i=1:nx
        horizon(4 + 12*(i-1)) = k_phi*(phiref(i) - phi) + phidot(i);
        horizon(5 + 12*(i-1)) = k_theta*(thetaref(i) - theta) + thetadot(i);
        
        phi = phi + horizon(4 + 12*(i-1))*dt;
        theta = theta + horizon(5 + 12*(i-1))*dt;
    end
    
    horizon(1:12:end) = UAV.alphass;
    horizon(2:12:end) = betaref(1:end-1);
    horizon(3:12:end) = vtref(1:end-1);
    horizon(6:12:end) = dpsiref(1:end-1);
    
    for i=7:12
         horizon(i:12:end) = aux(i);
    end
    
    if any(abs(horizon(4:12:end)) > 1)
        1;
    end
    
end