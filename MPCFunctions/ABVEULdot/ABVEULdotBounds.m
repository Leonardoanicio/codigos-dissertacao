function [Aeq, Beq, Aineq, Bineq] = ABVEULdotBounds(x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UAV)   

    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    AEQ = licols(AEQ')';
    Aeq = kron(eye(nu), AEQ);
    Beq = zeros(size(Aeq, 1), 1);
    
    
    if strcmp(UAV.Type, 'elevon')
        AINEQ2 = kron(eye(nu), [-0.5 0.5 0; 0.5 0.5 0; 0 0 1]*Coeffs_P*Beta);
        AINEQ1 = [-0.5 0.5 0; 0.5 0.5 0; 0 0 1]*Coeffs_P*Beta;
    else
        AINEQ2 = kron(eye(nu), Coeffs_P*Beta);
        AINEQ1 = Coeffs_P*Beta;
    end
    
    AINEQ1 = kron(tril(ones(nu, nu)), AINEQ1);
    BINEQ1U = repmat(UAV.ControlBounds(:, 1) - controls, nu, 1);
    BINEQ1L = repmat(UAV.ControlBounds(:, 2) - controls, nu, 1);
   
    sizeU = size(UAV.ControlBounds, 1);
    
    BINEQ2U = 0.01*ones(sizeU*nu, 1);
    BINEQ2L = -0.01*ones(sizeU*nu, 1);
    
    K1 = zeros(1, 12);
    K1(1, 1) = 1;
    K1 = kron(eye(nx), K1); %-> Seleciona alpha
    
    K2 = zeros(1, 12);
    K2(1, 3) = 1;
    K2 = kron(eye(nx), K2); %-> Seleciona vt
    
    K3 = zeros(1, 12);
    K3(1, 5) = 1;
    K3 = kron(tril(ones(nx, nx)), K3)*0.01; % -> Seleciona sum(dtheta)*dt
    
    K4 = zeros(1, 12);
    K4(1, 2) = 1;
    K4 = kron(eye(nx), K4); %-> Seleciona vt
    
    K = [K1; K2; K3; K4];
    
    AINEQ3 = K*S;
    
    BINEQ3U = [
                UAV.StateBounds(1, 1)*ones(nx, 1); 
                UAV.StateBounds(2, 1)*ones(nx, 1); 
                UAV.StateBounds(3, 1)*ones(nx, 1); 
                UAV.StateBounds(3, 1)*ones(nx, 1);] - K*T*x;
    BINEQ3L = [
                UAV.StateBounds(1, 2)*ones(nx, 1);
                UAV.StateBounds(2, 2)*ones(nx, 1);
                UAV.StateBounds(3, 2)*ones(nx, 1);
                UAV.StateBounds(3, 2)*ones(nx, 1);] - K*T*x;
    
    Aineq = [AINEQ1; -AINEQ1; AINEQ2; -AINEQ2; AINEQ3; -AINEQ3];
    Bineq = [BINEQ1U; -BINEQ1L; BINEQ2U; -BINEQ2L; BINEQ3U; -BINEQ3L];   
end