function [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT, Windout] = TrackingABVEULdot(MPC, ...
                                                            UAV, tf, dt, curveType, Wind, GustWind, EKF_USE)
                                               
    Hqp = MPC.Hqp;
    Fqp = MPC.Fqp;
    S = MPC.S;
    T = MPC.T;
    nx = MPC.nx;
    nu = MPC.nu;
    options = MPC.options;
    
    Windout = zeros(3, tf/dt);
    Wind0 = Wind;
    Winddot = zeros(3, 1);
    
    %Configurações do campo
    tbeta = 0;
    circDir = 1; % recalculated below
    fstate = 1;%'follow curve'; % recalculated below
    
    %Vetores para salvar os dados
    U = zeros(size(UAV.ControlBounds, 1), tf/dt);
    LinSys.DV = zeros(6, tf/dt);
    LinSys.X = zeros(12, tf/dt);
    LinSys.noise = zeros(6, tf/dt);
    LinSys.EstimatedXdot = zeros(6, tf/dt);
    LinSys.RealXdot = zeros(6, tf/dt);
    E = zeros(6, tf/dt);
    Re = zeros(6, tf/dt);
    X = zeros(12, tf/dt);
    Tout = zeros(1, tf/dt);
    
    %Informações do campo
    FInfo.nearo = zeros(1, tf/dt);
    FInfo.D = zeros(1, tf/dt);
    FInfo.state = zeros(1, tf/dt);
    FInfo.cirdir = zeros(1, tf/dt);
    FInfo.tbeta = zeros(1, tf/dt);
    FInfo.F = zeros(3, tf/dt);
    FInfo.Fc = zeros(3, tf/dt);
    FInfo.Fo = zeros(3, tf/dt);

    %Carrega os estados iniciais
    X(:, 1) = cell2mat(UAV.States);
    U(:, 1) = UAV.Controls;
    xdot = zeros(12, 1);
    GroundTruth = UAV.States;
    EKF_States = cell2mat(GroundTruth(1:9));
    controls = UAV.Controls;
    
    %EKF
    global EKF_P
    EKF_UAV = UAV;
    EKF_OUT = zeros(9, size(X, 2));
    EKF_OUT(:, 1) = EKF_States;
    EKF_Q = [0 0 0,...
             0.0025*9.81 0.0025*9.81 0.0025*9.81,...
             0.0026 0.0026 0.0026].^2;
    EKF_R = [0.05 0.05 0.2];
    EKF_Q = diag(EKF_Q);
    EKF_R = diag(EKF_R);
    
    t_correct = 120e-3;
    t_predict = 10e-3;
    
    %Simulacao
    i = 1;
    iw = 1;
    simdt = 1e-3;
     for t=0:simdt:tf
        %Calcula as matrizes do modelo real
        [Real_M, Real_C, ~, ~] = UAV.getMatrices();
        
         if i == 1
            oldGPS = cell2mat(GroundTruth(1:3));
            oldAccel = Real_M*controls + Real_C;
            oldAccel = oldAccel(1:3)/UAV.Coefficients.uavMass;
            oldGyro = cell2mat(GroundTruth(10:12));
            
            GPS = oldGPS;
        end
        
        if mod(t, t_predict) == 0 %Passo de predição
            Gyro = cell2mat(GroundTruth(10:12)); %Pega pqr
            
            Accel = Real_M*controls + Real_C;
            Accel = Accel(1:3)/UAV.Coefficients.uavMass; %Pega as acelerações lineares
            
            [EKF_P, EKF_States, EKF_xdot, oldGyro, oldAccel, ~] = EKF(EKF_P, EKF_States, Gyro, Accel, GPS,...
            oldGyro, oldAccel, oldGPS, EKF_R, EKF_Q, false, Wind0, 0);
        end
        if mod(t, t_correct) == 0%Passo de correção
            GPS = cell2mat(GroundTruth(1:3));
            [EKF_P, EKF_States, ~, ~, ~, oldGPS] = EKF(EKF_P, EKF_States, Gyro, Accel, GPS,...
            oldGyro, oldAccel, oldGPS, EKF_R, EKF_Q, true, Wind0, 0);
        end
        if mod(t, dt) == 0 %Calula o controle
            tic;
            
            States = num2cell([EKF_States; oldGyro]); %Adiciona PQR aos estados
            if EKF_USE
                EKF_UAV.States = States;
                EKF_UAV.Controls = controls;
            else
                States = GroundTruth;
                EKF_xdot = xdot;
                EKF_UAV = UAV;
            end
            
            %Calcula as matrizes do estimador
            [EKF_M, EKF_C, EKF_Pinv, ~] = EKF_UAV.getMatrices();

            %Distribui os estados
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = States{:};

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);
            
            %Feedback Linearization
            [Alpha, Beta] = FLABVEULdot(States, EKF_C, EKF_UAV.Coefficients);

            %Estados atuais -> modelo linear
            x = [alpha; beta; vt; EKF_xdot(7); EKF_xdot(8); EKF_xdot(9); Beta\(EKF_M*controls - Alpha)];
            LinSys.X(:, i) = x;

            %Horizonte de predição
            [xob, fstate, tbeta, circDir, nearObstacle, D, F, Fc, Fo] = ABVEULdotHorizon(States, nx, dt,...
                                                           fstate, tbeta, circDir, curveType, t, Wind0, x, EKF_UAV);
            
            
            FInfo.F(:, i) = F;
            FInfo.Fc(:, i) = Fc;
            FInfo.Fo(:, i) = Fo;
            FInfo.circdir(i) = circDir;
            FInfo.tbeta(i) = tbeta;
            FInfo.nearo(i) = nearObstacle;
            FInfo.D(i) = D;
            FInfo.state(i) = fstate;

            %Restricao dos estados
            [Aeq, Beq, Aineq, Bineq] = ABVEULdotBounds(x, T, S, EKF_Pinv, EKF_M, Beta, controls, ...
                                                        nx, nu, EKF_UAV);

            %Quadprog
            f = 2*(x'*T' - xob')*Fqp;
            if any(imag(f))
                break
            end
            [delta_v, flag] = mpcActiveSetSolver(Hqp, f', Aineq, Bineq, Aeq, Beq, false(size(Bineq)), options);
           
            Tout(i) = toc;

            if flag <= 0
                flag
                break
            end

            %Transforma para os controles
            controls = controls + EKF_Pinv*Beta*delta_v(1:6);
        end
        
        %Vento
        [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = GroundTruth{:};
        R = [
                cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
                sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
                -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
            ];
        Winddot = R'*GustWind(iw, :)'/dt;
        if iw > 1
            Winddot = R'*(GustWind(iw, :) - GustWind(iw-1, :))'/dt;
            Wind = Wind - R'*GustWind(iw-1, :)';
        end
        Wind = Wind + R'*GustWind(iw, :)';
        Windout(:, iw) = Wind;
        iw = iw + 1;
        

        %Evolui o modelo
        UAV.Controls = controls;
        [UAV, xdot] = UAV.EOM(simdt, Wind, Winddot);
        GroundTruth = UAV.States;

        if mod(i, 100) == 0
            clc;
            fprintf('Simulating\n%.2fs', t);
        end

        if mod(t, dt) == 0
            %Salva os dados
            U(:, i) = controls;
            EKF_OUT(:, i) = EKF_States;
            LinSys.DV(:, i) = delta_v(1:6);
            X(:, i) = cell2mat(GroundTruth); %-> Modelo não linear

            %Salva os erros
            Re(:,i) = xob(1:6);
            E(:,i) = xob(1:6) - x(1:6);
            i = i + 1; 
        end
    end
    
    if flag > 0
        clc;
    end
    fprintf('Max Time = %.4f\n', max(Tout));
    fprintf('Mean Time = %.4f\n', mean(Tout));
end