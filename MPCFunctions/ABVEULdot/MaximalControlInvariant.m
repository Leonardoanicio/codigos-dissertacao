function [Cinf, iter] = MaximalControlInvariant(X, U, model)
    Cinf = X;
    Cinfold = X;
    iter = 0;
    while true
        iter = iter + 1;
        Cinf = model.reachableSet('X', Cinfold, 'U', U, 'direction', 'backward', 'N', 1).intersect(Cinfold);
        
        if Cinf == Cinfold
            break
        end
        
        Cinfold = Cinf;
    end
end

