function [Aeq, Beq, Aineq, Bineq] = NEDdotEULdotBounds(theta, x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UAV)    
    
    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    AEQ = licols(AEQ')';
    Aeq = kron(eye(nu), AEQ);
    Beq = zeros(size(Aeq, 1), 1);
 
    dt = 0.01;
    
    %Restrição de theta
    Zthetadot = zeros(1, 12); 
    Zthetadot(5) = 1;
    Ytheta = kron(tril(ones(nx, nx)), Zthetadot);
    ThetaAineq = [Ytheta*S*dt; Ytheta*S*dt];
   
    Theta0 = repmat(theta, nx, 1);
    ThetaMax = repmat(pi/(2 + 1e-3), nx, 1);
    ThetaBineq = [-Ytheta*T*x*dt + ThetaMax - Theta0; Ytheta*T*x*dt + ThetaMax + Theta0];
    
    %Restrição de velocidade
    V0 = dot(x(1:3), x(1:3)) + dot(x(1:3), x(7:9))*2*dt;
    V0 = repmat(V0, nx, 1);
    V_stall = UAV.StateBounds(2 , 2)^2;
    V_stall = repmat(V_stall, nx, 1);
    ZV = zeros(1, 12);
    ZV(7:9) = x(1:3);
    ZV = kron(tril(ones(nx, nx)), ZV);
    VtAineq = -2*ZV*S*dt;
    VtBineq = V0 - V_stall + 2*dt*ZV*T*x;
    
    %Restrição da variação do sinal de controle
    sizeU = size(UAV.ControlBounds, 1);
    if strcmp(UAV.Type, 'elevon')
        Urate = kron(eye(nu), [-0.5 0.5 0; 0.5 0.5 0; 0 0 1]*Coeffs_P*Beta);
        Usum = kron(tril(ones(nu)), [-0.5 0.5 0; 0.5 0.5 0; 0 0 1]*Coeffs_P*Beta);
    else
        Urate = kron(eye(nu), Coeffs_P*Beta);
        Usum = kron(tril(ones(nu)), Coeffs_P*Beta);
    end
    DeltaVAineq = [Urate; -Urate];
    DeltaVBineq = [0.01*ones(sizeU*nu, 1); 0.01*ones(sizeU*nu, 1)];
    
    %Restrição do maximo do sinal de controle
    Usum = kron(tril(ones(nu)), Coeffs_P*Beta);
    U0 = repmat(controls, nu, 1);
    Umax = repmat(UAV.ControlBounds(:, 1), nu, 1);
    Umin = repmat(UAV.ControlBounds(:, 2), nu, 1);
    VAineq = [Usum; -Usum];
    VBineq = [Umax - U0; -Umin + U0];
    
    Aineq = [VAineq; DeltaVAineq; ThetaAineq; VtAineq];
    Bineq = [VBineq; DeltaVBineq; ThetaBineq; VtBineq];
end
