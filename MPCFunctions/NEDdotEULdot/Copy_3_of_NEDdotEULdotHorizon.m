function [horizon, stateOut, TGammaout, circOut, obstOut, D, Fout, fcOut, foOut] = NEDdotEULdotHorizon(states, nx, dt,...
                                                                state, tgamma, circDir, curveType, time, Wind, aux, UAV)
    horizon = zeros(12*nx, 1);
    
    k_psi = 1.5;
    k_theta = 1.5;
    k_phi = 1.5;
    
    [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    h = -pd;
    
    alpha = atan2(w, u);
    vt = norm([u; v; w]);
    beta = asin(v/vt);
    
%     alpha = UAV.alphass;
%     beta = 0;
    
    pos = [pn pe h]';
    
    [Fout, D, stateOut, TGammaout, circOut, obstOut, fcOut, foOut] = BugFieldAlpha(pos, curveType, state, tgamma, circDir, time, UAV.Type);  
    [F1, ~, FState, Tgamma, circDir] = BugFieldAlpha(pos, curveType, state, tgamma, circDir, time, UAV.Type);  
    
    psiref = zeros(nx + 1, 1);
    dpsiref = zeros(nx + 1, 1);
    thetaref = zeros(nx + 1, 1);
    phiref = zeros(nx + 1, 1);
    nedref = zeros(nx + 1, 3);
    cVec = zeros(nx + 1, 1);
    sVec = zeros(nx + 1, 1);
    
    pos = pos + F1*dt;
    
    [F1, ~, FState, Tgamma, circDir] = BugFieldAlpha(pos, curveType, FState, Tgamma, circDir, time + i*dt, UAV.Type);
        if norm(F1(1:2)) == 0
            [F1, ~, FState, Tgamma, circDir] = BugFieldAlpha(pos + [sin(psi); cos(psi); 0], curveType, FState, Tgamma, circDir, time + i*dt, UAV.Type);  
        end
        
        
        switch FState
            case 'follow curve'
                sVec(i) = 1;
            case 'transition to obstacle'
               sVec(i) = 2;
            case 'contour obstacle'
                sVec(i) = 3;
            case 'transition to curve'
               sVec(i) = 4;
        end
        cVec(i) = circDir;
        
        F2 = BugFieldAlpha(pos + F1*dt, curveType, FState, Tgamma, circDir, time + i*dt + dt, UAV.Type);      
        if norm(F2(1:2)) == 0
            F2 = BugFieldAlpha(pos + F1*dt + [sin(psi); cos(psi); 0], curveType, FState, Tgamma, circDir, time + i*dt + dt, UAV.Type);  
        end
        
        F1(3) = -F1(3);
%         F1 = F1 - Wind;
        F2(3) = -F2(3);
%         F2 = F2 - Wind;
        dF = (F2 - F1)/dt;
    
    for i=1:nx+1
        
        
        Fn = F1(1);
        Fe = F1(2);
        Fd = F1(3);
        
        Vd = Fd;
        
        %Body-NED
        R = [
                cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
                sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
                -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
            ];
        
        uvwref = R'*(F1);
        Vtref = norm(uvwref);
        
        F_psi = atan2(Fe, Fn);
        dF_psi = -Fe*dF(1)/(Fn^2 + Fe^2) + Fn*dF(2)/(Fn^2 + Fe^2);
        dpsi = k_psi*wrapToPi(F_psi - psi) + dF_psi;
        
        gamma = asin(-Vd/Vtref);
        g = dpsi*Vtref/9.81;
        
        a = 1 - g*tan(alpha)*sin(beta);
        b = sin(gamma)/cos(beta);
        c = 1 + g^2*cos(beta)^2;        
        tanphi = (g*cos(beta)*(a-b^2) + b*tan(alpha)*(c*(1 - b^2) + g^2*sin(beta)^2)^(1/2))/(cos(alpha)*(a^2 - b^2*(1 + c*tan(alpha)^2)));
        F_phi = atan(tanphi);

        a = cos(alpha)*cos(beta);
        b = sin(F_phi)*sin(beta) + cos(F_phi)*sin(alpha)*cos(beta);
        F_theta = (a*b + sin(gamma)*sqrt(a^2 - sin(gamma)^2 + b^2))/(a^2 - sin(gamma)^2);
        F_theta = atan(F_theta);

        
        F_phi = atan(dpsi*Vtref/9.81);
        F_theta = gamma;
        
        
        psi = psi + dpsi*dt;
        pos = pos + [F1(1); F1(2); -F1(3)]*dt;
         
        nedref(i, :) = F1 + Wind;
        dpsiref(i) = dpsi;
        psiref(i) = F_psi;
        phiref(i) = F_phi;
        thetaref(i) = F_theta;
    end    
    
    thetadot = diff(thetaref)/dt;
    phidot = diff(phiref)/dt;
    
    
    for i=1:nx
        horizon(4 + 12*(i-1)) = tanh(k_phi*(phiref(i) - phi)) + phidot(i);
        horizon(5 + 12*(i-1)) = tanh((k_theta*(thetaref(i) - theta)) + thetadot(i));
        
        phi = phi + horizon(4 + 12*(i-1))*dt;
        theta = theta + horizon(5 + 12*(i-1))*dt;
    end
    
    horizon(1:12:end) = nedref(1:end-1, 1);
    horizon(2:12:end) = nedref(1:end-1, 2);
    horizon(3:12:end) = nedref(1:end-1, 3);
    horizon(6:12:end) = dpsiref(1:end-1);
    
    for i=7:12
         horizon(i:12:end) = aux(i);
    end
    
end