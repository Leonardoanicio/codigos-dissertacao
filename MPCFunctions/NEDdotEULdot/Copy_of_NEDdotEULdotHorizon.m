function [horizon, stateOut, BetaOut, circOut, obstOut, D, Fout, fcOut, foOut] = NEDdotEULdotHorizon(states, nx, dt,...
                                                                state, tbeta, circDir, curveType, time, Wind)
    horizon = zeros(12*nx, 1);
    
    [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    h = -pd;
    
    alpha = atan2(w, u);
    vt = norm([u; v; w]);
    beta = asin(v/vt);
    
    pos = [pn pe h]';
    [Fout, D, stateOut, BetaOut, circOut, obstOut, fcOut, foOut] = BugFieldAlpha(pos, curveType, state, tbeta, circDir, time + dt);  
    
    pos = pos + Fout*dt;
    for i=1:nx
        [F1, ~, state, tbeta, circDir] = BugFieldAlpha(pos, curveType, state, tbeta, circDir, time + i*dt);      
        if norm(F1(1:2)) == 0
            [F1, ~, state, tbeta, circDir] = BugFieldAlpha(pos + [sin(psi); cos(psi); 0], curveType, state, tbeta, circDir, time + i*dt);  
        end
        
        Fx = F1(1);
        Fy = F1(2);
        Fz = F1(3);
        
        Vd = -Fz;
        
        %Body-NED
        R = [
                cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
                sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
                -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
            ];
        
        F2 = [F1(1); F1(2); -F1(3)];
        uvwref = R'*(F2 - Wind);
        Vtref = norm(uvwref);
        
        k_psi = 1;
        F_psi = atan2(Fy, Fx);
        dpsi = k_psi*wrapToPi(F_psi - psi);
        
        gamma = asin(-Vd/Vtref);
        g = dpsi*Vtref/9.81;
        
        a = 1 - g*tan(alpha)*sin(beta);
        b = sin(gamma)/cos(beta);
        c = 1 + g^2*cos(beta)^2;
        
        k_phi = 1.5;
        tanphi = (g*cos(beta)*(a-b^2) + b*tan(alpha)*(c*(1 - b^2) + g^2*sin(beta)^2)^(1/2))/(cos(alpha)*(a^2 - b^2*(1 + c*tan(alpha)^2)));
        F_phi = atan(tanphi);
        dphi = k_phi*(F_phi - phi);
        
        k_theta = 1;
        a = cos(alpha)*cos(beta);
        b = sin(phi)*sin(beta) + cos(phi)*sin(alpha)*cos(beta);
        F_theta = (a*b + sin(gamma)*sqrt(a^2 - sin(gamma)^2 + b^2))/(a^2 - sin(gamma)^2);
        F_theta = atan(F_theta);
        dtheta = k_theta*(F_theta - theta);
        
        psi = psi + dpsi*dt;
        theta = theta + dtheta*dt;
        phi = phi + dphi*dt;
        
        pos = pos + F1*dt;
        horizon((i-1)*12+1:12*i, 1) = [F2' dphi dtheta dpsi 0 0 0 0 0 0]';
    end
end