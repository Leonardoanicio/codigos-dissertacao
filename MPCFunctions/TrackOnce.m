function [Result] = TrackOnce(EKF_USE, WithWind, curveType, feedType, UAV)
    %Simulacao
    dt = 0.01;
    t_f = 200;

    %MPC
    nu = 2; 
    nx = 10;

    [Hqp, Fqp, S, T, options] = MPCInitialization(nx, nu, dt, feedType, UAV);
    MPC.Hqp = Hqp;
    MPC.Fqp = Fqp;
    MPC.S = S;
    MPC.T = T;
    MPC.nx = nx;
    MPC.nu = nu;
    MPC.options = options;

    global EKF_P;
    switch UAV.Type
        case 'elevon'
            va = 15;
            controls = [0 0 0.5]';
            load('EKF_P_ZAGI');
        otherwise
            va = 35;
            controls = [0 0 0 0.5]';
            load('EKF_P_AEROSONDE');
    end

    %Estados iniciais
    if curveType == 2
        states = [-600 600 -200 va 0 0 0 0.05 0 0 0 0]';
    else
        states = [400 400 -220 va 0 0 0 0.05 -pi 0 0 0]';
    end

    %Vento
    Wind = [3; 1; -0.5]*WithWind; %WIND IN NED FRAME

    %Dryden model, low altitude, light turbulence
    lw = 50; sigmaw = 0.7; 
    lu = 200; sigmau = 1.06;
    lv = 200; sigmav = 1.06;

    gu = tf(sigmau*sqrt(2*va/lu), [1 va/lu]);
    gv = tf(sigmav*sqrt(3*va/lv)*[1 va/(sqrt(3)*lv)], [1 2*va/lv (va/lv)^2]);
    gw = tf(sigmaw*sqrt(3*va/lw)*[1 va/(sqrt(3)*lw)], [1 2*va/lw (va/lw)^2]);

    t = 0:1e-3:t_f;
    w = wgn(length(t), 1, 1);

    yu = lsim(gu, w, t);
    yv = lsim(gv, w, t);
    yw = lsim(gw, w, t);

    GustWind = [yu, yv, yw]*WithWind*0;

    %Trimming inicial
    UAV.States = num2cell(states);
    UAV.Controls = controls;
    [UAV, xdot] = trim(UAV, zeros(3, 1), zeros(3, 1), dt);
    UAV.alphass = atan2(UAV.States{6}, UAV.States{4});

    %%
    switch feedType
        case "ABVEULdot"
            [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT, Windout] = TrackingABVEULdot(MPC,...
                                                                UAV, t_f, dt, curveType, Wind, GustWind, EKF_USE);
        case "NEDdotEULdot"
            [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT, Windout] = TrackingNEDdotEULdot(MPC,...
                                                                UAV, t_f, dt, curveType, Wind, GustWind, EKF_USE);
    end

    index = round((t-dt)/dt);
    t = linspace(0, t, index);
    %%
    if strcmp(UAV.Type, 'elevon')
        U = [-0.5 0.5 0; 0.5 0.5 0; 0 0 1]*U;
    end
    X = X(:, 1:index); %VANT
    EKF_OUT = EKF_OUT(:, 1:index); %EKF
    U = U(:, 1:index); %CONTROLES
    E = E(:, 1:index); %ERROS MPC
    Re = Re(:, 1:index); %REFERENCIA MPC
    LinSys.noise = LinSys.noise(: ,1:index); %IGNORA
    LinSys.X = LinSys.X(: ,1:index); %IGNORA

    FInfo.F = FInfo.F(:, 1:index);
    FInfo.state = FInfo.state(1:index); %INFORMAÇÕES DO CAMPO
    
    Result = {X; U; E; Re; EKF_OUT; FInfo};
end