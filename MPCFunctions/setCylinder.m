function setCylinder(OB, cx, cy, h, R)

[X, Y, Z] = cylinder([R R]);
X = X + cx; Y = Y + cy; Z = h*Z;



OB.XData = X;
OB.YData = Y;
OB.ZData = Z;

end